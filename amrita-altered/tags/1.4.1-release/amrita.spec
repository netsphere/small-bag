
%define name    amrita-altered
%define version 1.4.1
%define release 1.fc13


################################################
# introduction

%if !0%{?rbdir:1}
%define rbdir sitedir
%endif
%if !0%{?rblibdir:1}
%define rblibdir %(%{ruby} -rrbconfig -e "puts Config::CONFIG['%{rbdir}']")
%endif

Name:        %name
Summary:     A HTML/XML template library for Ruby.
Summary(ja): Ruby用のHTML/XMLテンプレートエンジン.
Version:     %version
Release:     %release
License:     Ruby's license
Group:       Development/Libraries
URL:         http://www.nslabs.jp/amrita-altered.rhtml
Requires:    ruby-devel >= 1.8.7

Source:      %{name}-%{version}.tar.gz
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root

BuildArchitectures: noarch
AutoReqProv: no
Prefix: %(%{ruby} -rrbconfig -e "puts Config::CONFIG['prefix']")

%description
amrita-altered is a HTML/XML template library for Ruby.

%description -l ja
amrita-altered は、HTML/XMLテンプレートエンジンです。
テンプレートには属性の形でマークを埋め込み、別途モデルデータを与えます。

%changelog
* Fri Aug 5 2002 Taku Nakajima <tnakajima@brain-tokyo.jp>
- added bin/ams

* Fri Jul 19 2002 Taku Nakajima <tnakajima@brain-tokyo.jp>
- apply patch from Nobu Nakata

* Tue Jul  9 2002 Taku Nakajima <tnakajima@brain-tokyo.jp>
- first release

################################################
%prep
%setup -q

################################################
%build

################################################
%install
rm -rf %{buildroot}

make %{?srcdir:-C %{srcdir}} install \
	PREFIX=$RPM_BUILD_ROOT%{prefix} SITE_DIR=$RPM_BUILD_ROOT%{rblibdir}


################################################
%clean
rm -rf %{buildroot}

################################################
%files
%defattr(-, root, root)

%{rblibdir}/amrita/compiler.rb
%{rblibdir}/amrita/format.rb
%{rblibdir}/amrita/tag.rb
%{rblibdir}/amrita/parser.rb
%{rblibdir}/amrita/node_expand.rb
%{rblibdir}/amrita/node.rb
%{rblibdir}/amrita/template.rb
%{rblibdir}/amrita/ams.rb
%{rblibdir}/amrita/xml.rb
%{rblibdir}/amrita/amx.rb
%{rblibdir}/amrita/handlers.rb
%{rblibdir}/amrita/cgikit.rb
%{rblibdir}/amrita/merge.rb
%{rblibdir}/amrita/parts.rb
%{prefix}/bin/ams
%{prefix}/bin/amx
%{prefix}/bin/amshandler
