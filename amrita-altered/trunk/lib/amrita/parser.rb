# -*- coding: utf-8 -*-

# Amrita -- A html/xml template library for Ruby.
# Copyright (c) 2002 Taku Nakajima.
# Licensed under the Ruby's License.

# Copyright (c) 2003-2005, maintenanced by HORIKAWA Hisashi.
#     http://www.nslabs.jp/amrita-altered.rhtml


require "amrita/node"
require "amrita/html_scanner"


module Amrita

  class HtmlParseError < StandardError
    attr_reader :error, :fname, :lno, :line

    def initialize(error, fname, lno, line)
      @error, @fname, @lno, @line = error, fname, lno, line
      super("error hapend in #{@fname}:#{@lno}(#{error}) \n==>#{line}")
    end
  end

  class Multi   #:nodoc: all
    attr_reader :alt

    def initialize(alt)
      @child_nodes = []
      @alt = alt
      @cur = 0
    end
    
    def <<(node)
      raise TypeError if !node.is_a?(TemplateNode)

      @child_nodes << node
      return self
    end
    
    def size()
      return @child_nodes.size
    end

    def inspect()
      "[alt=#{@alt}, " + @child_nodes.collect {|n| n.inspect()}.join(", ") + "]"
    end
  end

  # テンプレートで作られる仮想的な親子関係を持つ
  # ツリーを生成する。
  class TemplateNode  #:nodoc: all
    attr_reader(
      :child_nodes,
      :ref_node)        # Nodeオブジェクトへの参照の配列
    attr_accessor(
      :parent)          # 親ノード
    protected :parent=

    def initialize()
      @child_nodes = {}
    end

    def insert_child(name, t_node, alt)
      raise TypeError if !t_node.is_a?(TemplateNode)
      raise TypeError if !name.is_a?(String)
      t_node.parent = self
      
      if alt || @child_nodes[name]
        if @child_nodes[name].is_a?(Multi)
          @child_nodes[name] << t_node
        else
          m = Multi.new(alt)
          m << @child_nodes[name] if @child_nodes[name]
          m << t_node
          @child_nodes[name] = m
        end
        if t_node.ref_node.is_a?(Element)
          t_node.ref_node.multi = @child_nodes[name]
        end
      else
        @child_nodes[name] = t_node
      end
    end

    def set_ref(dnode)
      raise TypeError if !(dnode.is_a?(Element) || dnode.is_a?(Attr))
      @ref_node = dnode
      return self
    end

    def inspect()
      "<#{ref_str(@ref_node)}>, {" + 
      @child_nodes.collect {|k, v| "#{k} => " + v.inspect()}.join(", ") + "}"
    end

    private
    def ref_str(node)
      return "" if node.nil?
      raise TypeError, "expected a Node but #{node.class}" if !node.is_a?(Node)
      r = ""
      while true
        case node
        when Element
          sibling = node.parent.child_nodes.select {|v|
                                     v.is_a?(Element) && v.name == node.name}
          if sibling.size <= 1
            r = "/" + node.name + r
          else
            sibling.each_with_index {|s, i|
              if node.equal?(s)
                r = "/#{node.name}[#{i}]#{r}"
                break
              end
            }
          end
          node = node.parent
        when Attr
          r = "/@" + node.name + r
          node = node.element
        else
          break
        end
      end
      return r
    end
  end

  # HTMLパーサ兼テンプレート木を生成する
  class HtmlParser
    class << self
      def parse_inline(text, taginfo = DefaultHtmlTagInfo)
        c = caller(1)[0].split(":")
        parser = HtmlParser.new(text, c[0], c[1].to_i, taginfo)
        parser.parse
      end

      def parse_text text, fname = nil, lno = 0, taginfo = DefaultHtmlTagInfo, 
                     &filter
        parser = HtmlParser.new(text, fname, lno, taginfo, filter)
        parser.parse
      end
    
      def parse_io(io, fname = nil, lno = 0, taginfo = DefaultHtmlTagInfo)
        parser = HtmlParser.new(io.read(), fname, lno, taginfo)
        parser.parse
      end
    
      def parse_file(fname, taginfo = DefaultHtmlTagInfo)
        File.open(fname) {|f|
          HtmlParser.parse_io(f, fname, 0, taginfo)
        }
      end
    end

    attr_accessor(
      :tmpl_id,     # テンプレートエンジンで使う属性名
      :attr_style)  # 属性展開を要素の子とするか、並列とするか

    def initialize source, fname, lno, taginfo, filter = nil
      @scanner = HtmlScanner.new(source, taginfo)
      @taginfo = taginfo
      @tmpl_id = "amrita_id"
      @attr_style = "1.8"  # 属性は要素の子
      @filter = filter
    end

    def parse()
      root_doc = Element.new("<dmy_root>".intern)
      root_tmpl = TemplateNode.new()
      parse1(root_doc, root_tmpl)
      return root_doc.body
    end
    
  private
    def append_new_ref(parent_tnode, name, dnode)
      raise TypeError, "must be a Node, but #{dnode.class}" if !(dnode.is_a?(Element) || dnode.is_a?(Attr))
      raise TypeError if !name.is_a?(String)

      if name[-1] == ?+
        if dnode.attrs[@tmpl_id].value != name
          raise "template was invalid"
        else
          alt = true
          name = name[0..-2]
          dnode.attrs[@tmpl_id] = name
        end
      end

      t = TemplateNode.new()
      t.set_ref(dnode)
      parent_tnode.insert_child(name, t, alt)
      return t
    end
    
    def generate_element(tag)
      a = tag.attrs.collect {|k, v| Attr.new(k, v)}
      Element.new(tag.name.intern, *a)
    end

    def parse1(cur_doc_node, cur_tmpl_node)
      while true
        state, value = @scanner.scan()
        case state
        when :tag
          # valueはTagオブジェクト
          if value.start_tag?  # 開始タグ or 空要素タグ
            if @taginfo.accept_child?(cur_doc_node, value.name)
              child = generate_element(value)
              cur_doc_node.add(@filter ? @filter.call(child) : child)

              if child.attrs[@tmpl_id]
                # <foo tmplid="tmpl_name">
                t = append_new_ref(cur_tmpl_node, child.attrs[@tmpl_id].value, child)
                
                # <foo tmplid="tmpl_name1" bar="@tmpl_name2">
                child.attrs.each {|a|
                  if a.value && a.value[0] == ?@
                    if @attr_style < "1.8"
                      append_new_ref(cur_tmpl_node, a.value[1..-1], a)  # amrita.orig 1.0互換
                    else
                      append_new_ref(t, a.value[1..-1], a)
                    end
                  end
                }
              else
                # <foo bar="@tmpl_name">
                child.attrs.each {|a|
                  if a.value && a.value[0] == ?@
                    append_new_ref(cur_tmpl_node, a.value[1..-1], a)
                  end
                }
                t = cur_tmpl_node
              end
              parse1(child, t) if !value.empty_tag?
            else
              @scanner.push_back(state, value)
              return
            end
          else
            if cur_doc_node.tagname == value.name[1..-1]
              return
            else
              @scanner.push_back(state, value)
              return
            end
          end
        when :text
          cur_doc_node.add TextElement.new(value)
        when :special_tag
          cur_doc_node.add SpecialElement.new(value[0], value[1])
        when nil
          break
        else
          raise "unknown scanner_token #{state}"
        end
      end
    end
  end
  
end

