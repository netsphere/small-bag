# -*- coding: utf-8 -*-

# Amrita -- A html/xml template library for Ruby.
# Copyright (c) 2002 Taku Nakajima.
# Licensed under the Ruby's License.

# Copyright (c) 2003-2005, maintenanced by HORIKAWA Hisashi.
#     http://www.nslabs.jp/amrita-altered.rhtml


require 'strscan'
require "amrita/tag"


module Amrita

  class HtmlScanner  #:nodoc:all
    include Amrita

    class << self
      # ノードを得るたびにコールバックする
      def scan_text text, &block
        scanner = new(text)
        pos = -1
        loop do
          state, value = *(scanner.scan { text[pos+=1] })
          #puts ":#{state}:#{value.type}" 

          break unless state

          yield(state, value)
        end
      end
    end # << self


    # @param [String] src スキャンするテキスト
    def initialize src, taginfo = DefaultHtmlTagInfo
      @sc = StringScanner.new(src)
      @taginfo = taginfo
      @src_str = src
      @text = ""
      @tagname = ""
      @attrs = []
      @attrname = ""
      @attrvalue = ""
      @push_back_value = []
      @state = method(:state_text)
    end


    # 一つノードを得る
    def scan
      if @push_back_value.size > 0
	return @push_back_value.pop 
      end
      loop do
	return nil if @sc.empty?
	@pointer = @sc.pointer
	#next_text = @sc.peek(10)
	#puts "#{next_text}:#{state}:#{value}:#{@state}"
	state, value = *@state.call
	#puts "#{state}:#{value}:#{@state}"

	return [state, value] if state
      end
    end


    def push_back state, value
      @push_back_value.push([state, value])
    end

    def empty?
      @push_back_value.size == 0 and @sc.empty?
    end


    private

    def generate_tag
      @tagname.downcase!
      klass = @taginfo.get_tag_info(@tagname).tag_class || Tag
      ret = klass.new(@tagname, @attrs)
      @tagname = ""
      @attrs = []
      [:tag, ret]
    end

    NAME_RE = /([A-Za-z:_][\w.:-]*)/

    def state_text
      t = @sc.scan(/\A[^<]*/m)
      if t
	@state = method(:state_tagname)
	t.gsub!("&gt;", ">")
	t.gsub!("&lt;", "<")
	t.gsub!("&amp;", "&")
	t.gsub!("&quot;", '"') #"
	#t.gsub!("&nbsp;", " ")
        t.gsub!(/&#(\d+);/) { $1.to_i.chr } 
	if t.size > 0
	  [:text, t]
	else
	  nil
	end
      else
	[:text, @sc.scan(/\A.*/m)]
      end
    end

    def state_tagname
      l = @sc.skip(/\A</)
      raise "can't happen" unless l == 1
      
      @sc.skip(/\A\s+/m)
      if t = @sc.scan(/\A\/?#{NAME_RE}/)
	@state = method(:state_space)
	@tagname = t
	nil
      elsif t = @sc.scan(/\A!--|%=|%|\?|!/)
	@state = method(:state_special_tag)
	@tagname = t
	nil
      elsif t = @sc.scan(/\A[^>]+/m)
	@sc.skip(/\A>/)
	@tagname = t
	@state = method(:state_text)
	generate_tag
      else
	raise "can't happen"
      end
    end

    # <と>の間のスペース
    def state_space
      @sc.skip(/\A\s*/m)
      if @sc.scan(/\A>|\/>/)
	@state = method(:state_text)
	generate_tag
      elsif t = @sc.scan(/\A#{NAME_RE}/m)
	@attrname = t
	@state = method(:state_attrname)
	nil
      else
        raise "can't happen at #{@sc.pos}"	
      end
    end

    def state_attrname
      @sc.skip(/\A\s*/m)
      if t = @sc.scan(/\A#{NAME_RE}/m)
	@attrname = t
	@state = method(:state_before_equal)
	nil
      elsif t = @sc.scan(/\A=/)
	@state = method(:state_after_equal)
	nil
      elsif t = @sc.scan(/\A>|\/>/)
	@attrs << [@attrname, nil]
	@state = method(:state_text)
	generate_tag
      else
	raise "can't happen"	
      end
    end

    def state_before_equal
      @sc.skip(/\A\s*/m)
      if t = @sc.scan(/\A=/)
	@state = method(:state_after_equal)
	nil
      elsif t = @sc.scan(/\A>|\/>/) 
	@attrs << [@attrname, nil]
	@state = method(:state_text)
	generate_tag
      elsif t = @sc.scan(/\A#{NAME_RE}/)
	@attrs << [@attrname, nil]
	@attrvalue = ""
	@attrname = t
	@state = method(:state_attrname)
	nil
      else
	raise "can't happen"	
      end
    end

    def state_after_equal
      @sc.skip(/\A\s*/m)
      if t = @sc.scan(/\A"/) #"
	@state = method(:state_dqvalue)
	nil
      elsif t = @sc.scan(/\A'/) #'
	@state = method(:state_sqvalue)
	nil
      elsif t = @sc.scan(/\A>|\/>/)
	@attrs << [@attrname, nil]
	@state = method(:state_text)
	generate_tag
      elsif t = @sc.scan(/\A[^\s>]+/m)
	@attrs << [@attrname, t]
	@state = method(:state_space)
	nil
      elsif t = @sc.scan(/\A[^>]*/m)
	@attrs << [@attrname, t]
	@state = method(:state_attrname)
	nil
      else
	raise "can't happen"	
      end
    end
    
    def state_sqvalue
      t = @sc.scan(/\A[^']*/m) #'
      if t
        @attrs << [@attrname, t]
	@state = method(:state_space)
	@sc.skip(/\A'/) #'
	nil
      else
	raise "can't happen"	
      end
    end

    def state_dqvalue
      t = @sc.scan(/\A[^"]*/m) #"
      if t
        @attrs << [@attrname, t]
	@state = method(:state_space)
	@sc.skip(/\A"/) #"
	nil
      else
	raise "can't happen"	
      end
    end

    def state_special_tag
      re = end_tag_size = nil
      case @tagname
      when '%=', '%'
        re = /\A[^>]*%>/m
        end_tag_size = -2
      when '!--'
        re = /\A.*?-->/m 
        end_tag_size = -3
      when '?'
        re = /\A([^>]*)\?>/m
        end_tag_size = -2
	when '!'
        re = /\A([^>]*)>/m
        end_tag_size = -1
      else
        raise "can't happen"	
      end
      t = @sc.scan_until(re)
      raise "can't happen" unless t
      text = t[0...end_tag_size]
      @state = method(:state_text)
      [:special_tag, [@tagname, text]]
    end

    def current_line
      @sc.string[@pointer, 80]    
    end

    def current_line_no
      #done = @sc.string[0, @pointer]    
      done = @src_str[0, @pointer]    
      done.count("\n")
    end
  end

end # module Amrita
