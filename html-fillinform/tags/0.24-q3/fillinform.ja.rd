
=begin

= HTML::FillInForm

値が埋められた状態の HTML のフォームを簡単に作るモジュール

== HTML::FillInForm とは？

Web プログラミングにおける，デザインとロジックの分離を行うために
HTML を Ruby スクリプトで記述することなく値が入力済みのフォームを生成します．

Perl の HTML::FillInForm とほぼ同等の機能を持ちます．

== インストール

install.rb を使用しています

 % tar xvzf html-fillinform-x.xx.tar.gz
 % cd html-fillinform-x.xx
 % ruby install.rb config
 % ruby install.rb setup
 % su
 # ruby setup.rb install


== HTML::FillInForm クラス

=== クラスメソッド

--- HTML::FillInForm.new(html)
    新しい HTML::FillInForm オブジェクトを生成する．
    引数として与えられた HTML データを操作対象とします．

=== メソッド

--- HTML::FillInForm#load(file)
    引数として与えられたファイルを読み込み，操作対象とします．

--- HTML::FillInForm#set(html)
    引数として与えられた HTML データを操作対象とします．

--- HTML::FillInForm#fill(hash = {},name)
    第一引数には CGI オブジェクトのインスタンスもしくは，ハッシュを取ります．
    第二引数は form タグが複数ある場合のフォームの指定です．
    省略した場合は，HTML 中の全てのフォームが対象となります．
    戻り値は，値が埋められた HTML です．

--- HTML::FillInForm#output
--- HTML::FillInForm#to_s
    値を埋めた HTML を返します．

== サンプル

example ディレクトリ中の demo.cgi を参照して下さい．

== バグ/仕様

* フォーム関連のタグは小文字で出力されます．

* name,value,type の順番は決め打ちしてあるため、入れ換わる可能性があります．

== ライセンス,作者

著作権は IKEBE Tomohiro が保持します．

Ruby と同様のライセンスのもとで，自由に利用できます．

IKEBE Tomohiro <ikechin@0xfa.com>

=end

