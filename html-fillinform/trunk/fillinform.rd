
=begin

= HTML/FillInForm

create sticky HTML Forms with CGI.

== DESCRIPTION

Automatically inserts form data into HTML (ie. input,textarea,option etc..)

Just like a Perl's one.

== INSTALL

using install.rb 

 % tar xvzf html-fillinform-x.xx.tar.gz
 % cd html-fillinform-x.xx
 % ruby install.rb config
 % ruby install.rb setup
 % ruby test/test.rb
 % su
 # ruby install.rb install

== CLASS METHOD

--- HTML::FillInForm.new(html = nil)
create a new HTML::FillInForm object.

== METHODS

--- HTML::FillInForm#load(file)
load HTML file which contains Form.

--- HTML::FillInForm#set(html)
set HTML data which contains Form.

--- HTML::FillInForm#fill(hash,target = nil)
parse HTML and fill in Form with hash. you can also use cgi.rb object or any hash style object.
return value is a Form filled HTML.
if target is specified it will only work inside <FORM name="target"> .. </FORM>

--- HTML::FillInForm#output
--- HTML::FillInForm#to_s
return Form filled HTML.

== EXAMPLES

please see the example directory under a distribution package.

== BUGS

* All HTML tags are converted to lower case. 

* The order of attributes are not saved.

== AUTHOR

Copyright 2003 IKEBE Tomohiro 

This library is free software; you can redistribute it and / or modify it under the same terms as
Ruby itself.

IKEBE Tomohiro <ikechin@0xfa.com>

=end
