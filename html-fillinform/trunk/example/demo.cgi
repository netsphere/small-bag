#!/usr/local/bin/ruby

$:.unshift "./lib"
require "cgi"
require "erb"
require "html/fillinform"

class FillInFormDemo
  def initialize(cgi)
    @cgi = cgi
    f = open("./demo.rhtml")
    @erb = ERB.new(f.read)
    f.close
  end

  def run
    result = @erb.result(binding)
    if ENV["REQUEST_METHOD"] == "POST"
      fif = HTML::FillInForm.new(result)
      result = fif.fill(@cgi)
    end
    print @cgi.header("charset" => "euc-jp")
    print result
  end
end


app = FillInFormDemo.new(CGI.new)
app.run
