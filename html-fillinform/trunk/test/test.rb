
$:.unshift File.dirname(File.expand_path($0)).gsub!(%r#[^/]+$#,'') + "lib"
Dir.chdir( File.dirname($0) )

require 'html/fillinform'
require 'test/unit'

class FillInFormTest < Test::Unit::TestCase

  def setup
    fp = open('form.html')
    @html = fp.read
    fp.close
  end

  def test_target
    fif = HTML::FillInForm.new(@html)
     hash = {'username' => 'foobar'}
     assert_match(%r/<INPUT name="username" type="text" value="">/i, fif.fill(hash,'form5'))
  end

  def test_text
    fif = HTML::FillInForm.new(@html)
    hash = { 
      "username" => "bar",
      "password" => "baz",
      "submit" => nil,
    }
    assert_match(
		 %r/<input name="username" type="text" value="bar">/i,
		 fif.fill(hash)
		 )
    assert_match(
		 %r/<input name="password" type="password" value="baz">/i,
		 fif.fill(hash)		 
		 )
  end


  def test_submit
    fif = HTML::FillInForm.new(@html)
    hash = {}
    assert_match(
		 %r/<input name="submit" type="submit" value=" send ">/i,
		 fif.fill(hash)
		 )
  end

  def test_backward
    assert_nothing_raised(LoadError){
      require 'html/fillform'
    }
    fif = HTML::FillForm.new(@html)
    hash = { "username" => "baz" }
    assert_match(
		 %r/<input name="username" type="text" value="baz">/i,
		 fif.fill(hash)
		 )
  end

  def test_checkbox
    fif = HTML::FillInForm.new(@html)
    hash = {"hoge" => ["fuga"]}
    fif.fill(hash)
    assert_match(
		 %r/<input checked name="hoge" type="checkbox" value="fuga">/i,
		 fif.output
		 )
    assert_match(
		 %r/<input name="hoge" type="checkbox" value="hogehoge">/i,
		 fif.output
		 )
    assert_match(
		 %r/<input name="hoge" type="checkbox" value="xxxx">/i,
		 fif.output
		 )
  end

  def test_escape 
    fif = HTML::FillInForm.new(@html)
    hash = {
      "textbox" => %Q(<font size="1">foobar</font>), # textarea
      "username" => %Q(<font size="3">username</font>) # type="text"
    }
    assert_match(%r(&lt;font size=&quot;1&quot;&gt;foobar&lt;/font&gt;),fif.fill(hash));
    assert_match(%r(&lt;font size=&quot;3&quot;&gt;username&lt;/font&gt;),fif.output);
  end

  def test_option
    fif = HTML::FillInForm.new(@html)
    hash = {"selector" => "Ruby"}
    output =  fif.fill(hash)
    assert_match(
		 %r/<option selected>Ruby/i,
		 output
		 )
    assert_match(
		 %r/<option>Perl/i,
		 output
		 )
    assert_match(
		 %r/<option value="Python">Python/i,
		 output
		 )
  end

  def test_multiple
    fif = HTML::FillInForm.new(@html)
    hash = {
      "selector" => ["Ruby","Perl"],
      "hoge" => ["fuga","xxxx"],
      "test" => "1"
    }
    output =  fif.fill(hash)
    assert_match(
		 %r/<option selected>Ruby/i,
		 output
		 )
    assert_match(
		 %r/<option selected>Perl/i,
		 output
		 )
    assert_match(
		 %r/<option value="Python">Python/i,
		 output
		 )
    assert_match(
		 %r/<option selected value="1">one/i,
		 output
		 )
    assert_match(
		 %r/<input checked name="hoge" type="checkbox" value="fuga">/i,
		 fif.output
		 )
    assert_match(
		 %r/<input name="hoge" type="checkbox" value="hogehoge">/i,
		 fif.output
		 )
    assert_match(
		 %r/<input checked name="hoge" type="checkbox" value="xxxx">/i,
		 fif.output
		 )
  end

  def test_null
    fif = HTML::FillInForm.new(@html)
    hash = {}
    output = fif.fill(hash)
    assert_match(%r/<INPUT name="username" type="text" value=""><BR>/i,output)
  end

  def test_new
    fif = HTML::FillInForm.new
    assert_instance_of(HTML::FillInForm, fif)
  end

  def test_htmlsplit
    assert_nothing_raised(LoadError){
          require 'htmlsplit'
    }
  end

  def test_downcase
    fif = HTML::FillInForm.new(@html)
    hash = {'radio2' => "foo"}
    output = fif.fill(hash)
    assert_match(%r/<input checked name="radio2" type="RADIO" value="foo">/, 
		 output)
  end

end

require 'test/unit/ui/console/testrunner'
Test::Unit::UI::Console::TestRunner.run(FillInFormTest)

#RUNIT::CUI::TestRunner.run(FillInFormTest.suite)
