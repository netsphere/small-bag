
# FillInFormのテスト

require "html/fillinform"
require "runit/testcase"
require "runit/cui/testrunner"

class Test2 < RUNIT::TestCase
  # action属性のないformタグはエラー
  def test1
    fif = nil
    assert_no_exception() {
      fif = HTML::FillInForm.new("<form></form>")
    }
    assert_exception(RuntimeError) {
      fif.fill({})
    }
  end

  # タグが見つからないときは，無視
  def test2
    s = <<EOF
<form action="./t.rb">
<input name="t" type="text">
</form>
EOF
    fif = HTML::FillInForm.new(s)
    assert_no_exception() {
      fif.fill({"foo" => "bar"})
    }
  end

  # input[type = "text"]
  def test4
    s = <<EOF
<form action="./t.rb">
<input name="t" type="text">
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<input name="t" type="text">
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({"t" => "foo"})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<input name="t" type="text" value="foo">
</form>
EOF

    s = <<EOF
<form action="./t.rb">
<input name="t" type="text" value="bar">
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<input name="t" type="text" value="bar">
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({"t" => 100})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<input name="t" type="text" value="100">
</form>
EOF
  end

  # select
  def test5
    s = <<EOF
<form action="./t.rb">
<select name="t">
  <option>v1
  <option selected>v2
</select>
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<select name="t">
  <option>v1
  <option selected>v2
</select>
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({"t" => "v1"})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<select name="t">
  <option selected>v1
  <option>v2
</select>
</form>
EOF

    s = <<EOF
<form action="./t.rb">
<select name="t">
  <option value="1">v1
  <option value="2">v2
</select>
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<select name="t">
  <option value="1">v1
  <option value="2">v2
</select>
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({"t" => "2"})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<select name="t">
  <option value="1">v1
  <option selected value="2">v2
</select>
</form>
EOF
  end

  # '<'や'>'はエスケープされる
  def test6
    s = <<EOF
<form action="./t.rb">
<input name="t">
<textarea name="u"></textarea>
</form>
EOF
    fif = HTML::FillInForm.new(s)
    fif.fill({"t" => "<\">", "u" => "<\">"})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb">
<input name="t" value="&lt;&quot;&gt;">
<textarea name="u">&lt;&quot;&gt;</textarea>
</form>
EOF
  end

  def test7
    s = <<EOF
<form action="./t.rb"><input type="checkbox" name="t"></form>
EOF
    fif = HTML::FillInForm.new(s)
    assert_exception(TypeError) {fif.fill({"t" => true})}

    fif = HTML::FillInForm.new(s)
    fif.fill({"t" => "on"})
    assert_equal(<<EOF, fif.output)
<form action="./t.rb"><input checked name="t" type="checkbox"></form>
EOF
  end
end

RUNIT::CUI::TestRunner.run(Test2.suite)

def test1
  fif = HTML::FillInForm.new(<<EOF)
<html>
<form action="./fil.rb">
<input type="password" name="c2a" value="v"> <input type="password" name="c2b">

<input type="checkbox" name="c3a" checked> <input type="checkbox" name="c3aa">
<input type="checkbox" name="c3b">
<input type="checkbox" name="c3c" checked>

<input type="radio" name="c4a" value="s1"> <input type="radio" name="c4a" checked value="s2">
<input type="radio" name="c4b" checked value="s1"> <input type="radio" name="c4b" value="s2">

<input type="hidden" name="c5a" value="hoge"> <input type="hidden" name="c5b">

<input type="file" name="c6a"> <input type="file" name="c6b">

<textarea name="c8a">
ふにふに
</textarea>
<textarea name="c8b">
</textarea>
<textarea name="c8bb">
foo
</textarea>

<button name="c9a" value="foo">ボタン</button>
<button name="c9b" value="foo">ボタン</button>

</form>
</html>
EOF
  fif.fill({
    "c1b" => "bar",         # ok
    "c1bb" => "",
    "c2b" => "hoge",        # ok
    "c3b" => "on",
    "c3c" => "off",
    "c4b" => "s2",
    "c5b" => "baz",
    "c6b" => "config.sys",
    "c7b" => "v2",
    "c7bb" => "1",
    "c8b" => "bar",
    "c8bb" => "baz",
    "c9b" => "bar"
  })
  print "Content-Type:text/html\n\n"
  print fif.output
end

#test1
