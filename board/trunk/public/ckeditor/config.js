/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 *
 * エディタの設定ファイル
 */

CKEDITOR.editorConfig = function( config ) 
{
  //alert('hoge');

  // see http://drupal.org/node/669114
  config.autoParagraph = false;

	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
