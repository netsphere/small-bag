# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attach_files", force: true do |t|
    t.integer  "post_id",                       null: false
    t.string   "original_filename", limit: 250, null: false
    t.string   "content_type",      limit: 60,  null: false
    t.binary   "body",                          null: false
    t.datetime "created_at",                    null: false
    t.integer  "create_user_id",                null: false
  end

  create_table "boards", force: true do |t|
    t.integer  "user_group_id",             null: false
    t.string   "name",          limit: 100, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.integer  "board_id",   null: false
    t.integer  "user_id",    null: false
    t.text     "body",       null: false
    t.integer  "reply_to"
    t.datetime "created_at", null: false
  end

  create_table "user_groups", force: true do |t|
    t.string   "urlkey",     limit: 30, null: false
    t.string   "name",       limit: 40, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at"
  end

  add_index "user_groups", ["urlkey"], name: "user_groups_urlkey_key", unique: true, using: :btree

  create_table "user_groups_users", force: true do |t|
    t.integer  "user_group_id",            null: false
    t.integer  "user_id",                  null: false
    t.string   "email_addr",    limit: 40, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at"
  end

  add_index "user_groups_users", ["user_id", "user_group_id"], name: "user_groups_users_user_id_user_group_id_key", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "login",      limit: 40, null: false
    t.string   "name",       limit: 40, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at"
  end

  add_index "users", ["login"], name: "users_login_key", unique: true, using: :btree

end
