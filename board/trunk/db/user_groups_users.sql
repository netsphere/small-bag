
-- ユーザグループとユーザとの関係 (多対多)
CREATE TABLE user_groups_users (
  id            serial PRIMARY KEY,
  user_group_id int NOT NULL REFERENCES user_groups (id),
  user_id       int NOT NULL REFERENCES users (id),

  -- メール配信用
  email_addr    VARCHAR(40) NOT NULL,

  created_at    TIMESTAMP NOT NULL,
  updated_at    TIMESTAMP,
  UNIQUE (user_id, user_group_id)
);
