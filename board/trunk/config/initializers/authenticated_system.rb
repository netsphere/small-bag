# -*- mode:ruby; coding:utf-8 -*-

# Be sure to restart your server when you modify this file.


module AuthenticatedSystem
  Application = ::WebBoard::Application
  UserModel = ::User
  AuthTokenModel = ::Accounts::AuthToken
end
