# -*- coding:utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "rails_sup/authenticated_system/controller_helper"


class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include AuthenticatedSystem::ControllerHelper
  include ApplicationHelper

  # remember me
  before_filter :login_from_cookie


  # @override
  def redirect_to_login
    #flash[:alert] = "ログインしてください。" # + request.request_uri

    # request is a ActionDispatch::Request
    #session[:return_to] = 
    #        ::WebBoard::Application.config.app_root_url + request.fullpath
    redirect_to ::WebBoard::Application.config.accounts_app_url + 
                           "/account/sso?return_to=" + request.original_url
    # return false
  end


  # for before_filter
  def board_must_be_owned_by_ug
    if current_user
      u = current_user.is_a?(::User) ? current_user : 
                                       User.find_by_login(current_user.login)
    else
      render :action => "forbidden", :status => 403
      return false
    end

    board = Board.find( controller_name == "boards" ? 
                                            params[:id] : params[:board_id] )
    g = UserGroupsUser.where( "user_id = ? AND user_group_id = ?",
                              u.id, board.user_group.id ).first
    if g
      true
    else
      render :action => "forbidden", :status => 403
      return false
    end
  end

end
