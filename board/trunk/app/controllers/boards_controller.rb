# -*- coding: utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 掲示板 (PC用)。中心となるコントローラ
class BoardsController < ApplicationController
  before_filter :login_required
  before_filter :board_must_be_owned_by_ug

  verify :method => :post, :only => [ :update, :destroy ],
         :redirect_to => {:controller => "welcome", :action => "index"},
         :add_flash => {"alert" => "HTTP method incorrect."}

=begin
  def index
    @boards = Board.all
  end
=end

  
  # 投稿の表示
  def show
    @board = Board.find params[:id]
    @post = Post.new

    # 新しい順にレコードを得て、
    @posts = Post.where("board_id = ?", @board.id) \
                 .paginate(:page => params[:page], :per_page => 25) \
                 .order("created_at DESC")

    # 古い順にスレッドを作る
    rpo = []
    @posts.each do |post| rpo.unshift post end

    @threads = ThreadTree.new nil
    rpo.each do |post|
      if post.reply_to
        parent_node = @threads.find post.reply_to
        if parent_node
          parent_node << post
        else
          @threads << post
        end
      else
        @threads << post
      end
    end
  end


=begin
  # GET /boards/new
  # GET /boards/new.xml
  def new
    @board = Board.new
  end


  # POST /boards
  # POST /boards.xml
  def create
    @board = Board.new(params[:board])

    if @board.save
      redirect_to(@board, :notice => 'Board was successfully created.') 
    else
      render :action => "new" 
    end
  end
=end


  # GET /boards/1/edit
  def edit
    @board = Board.find(params[:id])
  end


  # PUT /boards/1
  # PUT /boards/1.xml
  def update
    @board = Board.find(params[:id])

    respond_to do |format|
      if @board.update_attributes(params[:board])
        format.html { redirect_to(@board, :notice => 'Board was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @board.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /boards/1
  # DELETE /boards/1.xml
  def destroy
    @board = Board.find(params[:id])
    @board.destroy

    respond_to do |format|
      format.html { redirect_to(boards_url) }
      format.xml  { head :ok }
    end
  end


  # 添付ファイルを表示
  def attach
    send_file "/path/to.zip", :type => "image/jpeg", :disposition => "inline"
  end
end
