#!/usr/local/bin/ruby
# -*- coding:utf-8 -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# プロフィールの変更

require "path-conf.rb"
require APP_ROOT + "/app/board-sub"
require "html/template"
require "html/fillinform"
require "cgisup"
require "ostruct"
require "digest/sha2"

$SAFE = 1


def render_edit cgi, profile, error = nil
  tmpl = HTML::Template.new APP_ROOT + "/templates/setting.html"
  tmpl_data = {
    "userid" => profile.userid
  }
  tmpl_data["if-admin"] = { } if profile.admin
  tmpl_data["if-alert"] = {"message" => error} if error

  tmpl.expand tmpl_data

  fillin = HTML::FillInForm.new(tmpl.output)
  cgi_header_out cgi
  print fillin.fill({
                      "name" => profile.name,
                      "website" => profile.website,
                      "mail" => profile.mail,
                      "mail2" => profile.mob_mail
                    })
end


def m userid, cgi
  raise TypeError if !userid.is_a?(String)

  profile = get_profile userid
  raise RuntimeError if !profile
=begin
  if !profile
    profile = OpenStruct.new :name => "", 
                             :website => "", 
                             :mail => "", 
                             :mob_mail => ""
  end
=end

  render_edit cgi, profile
end


def update_profile userid, cgi
  raise ArgumentError if !userid

  prof = nil
  StrStore.new(PRIV_DIR + "/kname").transaction do |profdb|
    prof = Profile.parse userid, profdb[userid]
    prof.name = cgi['name'][0]
    prof.website = cgi['website'][0]
    prof.mail = cgi['mail'][0]
    prof.mob_mail = cgi['mail2'][0]

    if cgi["admin-pwd"][0].to_s != ""
      if cgi["admin-pwd"][0].to_s != cgi["admin-pwd-confirm"][0].to_s
        render_edit cgi, prof, "管理者パスワード(確認) が一致しません"
        exit
      end
      prof.admin_hashed_pwd = Digest::SHA256.hexdigest cgi["admin-pwd"][0]
    end

    profdb[userid] = prof.to_s
  end

  tmpl = HTML::Template.new APP_ROOT + "/templates/setting2.html"
  tmpl.expand({
                "userid" => userid,
                "name" => CGI.escapeHTML(prof.name),
                "website" => CGI.escapeHTML(prof.website),
                "mail" => CGI.escapeHTML(prof.mail),
                "mail2" => CGI.escapeHTML(prof.mob_mail),
                "admin" => (prof.admin ? "管理者" : "-") })
  cgi_header_out cgi
  print tmpl.output
end


# main
begin
  cgi = CGI.new
  userid = ENV['REMOTE_USER']
  access_log userid
  prof = check_logined cgi, userid

  case cgi.get("a")
  when "set"
    if cgi.request_method != "POST"
      redirect_to cgi, "./"
      exit
    end
    update_profile userid, cgi
  else
    m userid, cgi
  end
rescue ScriptError, StandardError
  error_bt_out
end
