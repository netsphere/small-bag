#!/usr/local/bin/ruby
# -*- coding:utf-8; mode:ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "path-conf.rb"
require APP_ROOT + "/app/board-sub"
require "ftools"
require "message"
require "digest/sha2"

$SAFE = 1
ENV['PATH'] = '/bin'


# @return パスワードが一致する場合 true
def check_pwd prof, pwd
  return prof.admin &&
         prof.admin_hashed_pwd == Digest::SHA256.hexdigest(pwd)
end


def result_out cgi, ary
  html_header_out cgi, "deleted"
  print strgets(1)
  ary.each {|mid|
    print "<li>", mesno(mid), "\n"
  }
  print "</ul>\n"
  html_footer_out()
end


def entries_delete cgi, prof
  if !check_pwd(prof, cgi["pwd"].first)
    set_flash cgi, "alert", "管理者パスワードが違います。"
    redirect_to cgi, "./"
    exit
  end

  # るエントリを削除 (複数可)
  ary = cgi["del"]
  ary.each do |mid|
    File.move PRIV_DIR + "/data/" + mesno(mid), 
              PRIV_DIR + "/deleted/" + mesno(mid)
  end

  result_out cgi, ary
end


# main
begin
  cgi = CGI.new
  userid = ENV['REMOTE_USER']
  access_log userid
  prof = check_logined cgi, userid

  if cgi.request_method != "POST"
    redirect_to cgi, "./"
    exit
  end

  entries_delete cgi, prof
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end
