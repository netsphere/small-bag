#!/usr/local/bin/ruby
# -*- mode:ruby; coding:utf-8 -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "path-conf.rb"
require APP_ROOT + "/app/board-sub"
require "common"
require "cgisup"

$SAFE = 1
ENV['PATH'] = '/bin'


def do_search cgi
  text = cgi['text'].first || ""
  # text.gsub! /[ 　\t\r\n]+/, ' '
  if text.strip == ""
    set_flash cgi, "alert", "検索テキストがありません。"
    redirect_to cgi, "./"
    exit
  end

  html_header_out cgi, "search result"
  print strgets(21)
  print "<b>text:</b>", CGI.escapeHTML(text)

  counter = get_counter(PRIV_DIR + "/counter")
  while counter >= 0
    begin
      entry = get_entry counter
    rescue Errno::ENOENT
      counter -= 1
      next
    end
=begin
    fname = PRIV_DIR + "/data/" + sprintf("%04d", counter)
    begin
      if File.stat(fname).size <= 0
        counter -= 1
        next
      end
    rescue # ENOENT
      counter -= 1
      next 
    end

    fp = File.open(fname, "r")

    # header
    while line = fp.gets
      break if line.gsub(/[\r\n]/, '') == ""
    end
=end
    found = false
    entry.body.each do |line|
      # line.gsub! /[ 　\t\n\r]+/, ' '
      if /#{text}/iu =~ line
        if found == false
          printf "<p><a href=\"./index.rb?action=follow&amp;article=%d#%s\">%s</a><br>\n", counter, sprintf("%04d", counter), sprintf("%04d", counter)
          found = true
        end
        print CGI.escapeHTML($`), "<font color=\"red\">",
              CGI.escapeHTML($&), "</font>",
              CGI.escapeHTML($'), "<br>\n"
      end
    end
    # fp.close
    counter -= 1
  end

  print "<p><a href=\"./index.rb\">", strgets(22), "</a>"
  html_footer_out()
end


# main
begin
  cgi = CGI.new
  userid = ENV['REMOTE_USER']
  access_log userid 
  prof = check_logined cgi, userid

  do_search cgi
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end
