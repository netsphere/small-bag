# -*- coding:utf-8; mode:ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require APP_ROOT + "/config/board-conf"

# ruby 1.6 では CGI#[]メソッドは配列を返す. 
$COMPAT_VERSION = '1.6'


require "html/template"
require "html/fillinform"
require "htmlrepair"
require "strstore"
require "common"
require "datesup"
require "cgisup"
require "message"
require "csconv"
require "time"   # Time#iso8601
require APP_ROOT + "/app/strgets"
require APP_ROOT + "/app/profile-model"


MDB = MessageSpace.new PRIV_DIR + "/data"


def redirect_to cgi, url
  puts cgi.header("status" => "REDIRECT", 
                  "Location" => url)
  puts "redirected."
end


def set_flash cgi, key, message
  raise ArgumentError if key != "notice" && key != "alert"

  session = CGI::Session.new cgi, "tmpdir" => PRIV_DIR + "/tmp"
  session[key] = message
end


def get_flash cgi, key
  raise ArgumentError if key != "notice" && key != "alert"

  session = CGI::Session.new cgi, "tmpdir" => PRIV_DIR + "/tmp"
  if session[key]
    r = session[key]
    session[key] = nil
  end
  return r
end


# ログイン状態か確認し、ログインしていなければリダイレクトする。
# @return Profileインスタンス
def check_logined cgi, userid
  if !userid
    redirect_to cgi, "/"
    exit
  end

  prof = nil
  StrStore.new(PRIV_DIR + "/kname").transaction do |profdb|
    if !profdb.key?(userid)
      prof = Profile.new :userid => userid,
                         :name => "(名無し)",
                         :last_access_time => Time.now
    else
      prof = Profile.parse userid, profdb[userid]
    end

    prof.last_access_time = Time.now.iso8601
    profdb[userid] = prof.to_s
  end

  return prof
end


def uri_re
  alphanum      = "[0-9A-Za-z]"
  escaped       = "(%[0-9A-Fa-f][0-9A-Fa-f])"
  mark          = "[-_.!~*'()]"
  unreserved    = "(?:#{alphanum}|#{mark})"
  reserved      = "[;/?:@&=+$,]"
  uric          = "(?:#{reserved}|#{unreserved}|#{escaped})"
  fragment      = "(#{uric}*)"
  query         = "(#{uric}*)"
  pchar         = "(?:#{unreserved}|#{escaped}|[:@&=+$,])"
  param         = "(#{pchar}*)"
  segment       = "(#{pchar}*(?:;#{param})*)"
  abs_path      = "((/#{segment})+)"
  uric_no_slash = "(?:#{unreserved}|#{escaped}|[;?:@&=+$,])"
  opaque_part   = "(#{uric_no_slash}(?:#{uric})*)"
  port          = "([0-9]+)"
  ipv4address   = "([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)"
  toplabel      = "([A-Za-z]([-0-9A-Za-z]*#{alphanum})?)"
  domainlabel   = "(#{alphanum}([0-9A-Za-z-]*#{alphanum})?)"
  hostname      = "((?:#{domainlabel}\\.)*#{toplabel})"
  host          = "(?:#{hostname}|#{ipv4address})"
  hostport      = "(#{host}(?::#{port})?)"
  userinfo      = "((?:#{unreserved}|#{escaped}|[;:&=+$,])+)"
  server        = "((?:#{userinfo}@)?#{hostport})"
  scheme        = "([A-Za-z][A-Za-z0-9+.-]*)"
  net_path      = "(//#{server}(?:#{abs_path})?)"
  hier_part     = "(#{net_path}(?:\\?#{query})?)"
  absoluteURI   = "(#{scheme}:(?:#{hier_part}|#{opaque_part}))"
  uri_reference = "#{absoluteURI}(?:##{fragment})?"
  /#{uri_reference}/
end


def cgi_header_out cgi
  print cgi.header("type" => "text/html", 
                   "charset" => "UTF-8",
                   "Cache-Control" => "no-cache",
                   "Pragma" => "no-cache")
  print "\n"
end


def html_header_out cgi, title
  cgi_header_out cgi
  print <<EOF
#{sprintf(strgets(3), title)}
</head>
<body>
EOF
end


def html_footer_out
  print <<EOF
<hr />
<p align="right" style="margin-top:0">Powered by Q's WebBoard.<br>
Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved<a name="tail">.</a>
</body>
</html>
EOF
end #'


def html_formatter(msg)
  r = HTMLSplit.new(msg.body.join("\n"))
  r.repair
  print r.document
end


# 本文 (text/plain)
def plain_formatter msg
  msg.body.each {|line|
    line.gsub!(/[\r\n]/, '')
    line2 = ""
    while line =~ uri_re
      line2 += CGI.escapeHTML($`)
      line2 += "<a href=\"#{CGI.escapeHTML($&)}\" target=\"_top\">#{CGI.escapeHTML($&)}</a>"
      line = $'
    end
    line = line2 + CGI.escapeHTML(line)

    line2 = ""
    em_count = 0
    while line =~ /&lt;em[ \t]*&gt;/i
      line2 += $` + "<em>"
      line = $'
      em_count += 1
      if line =~ /&lt;\/em[ \t]*&gt;/i
        line2 += $` + "</em>"
        line = $'
        em_count -= 1
      end
    end
    line = line2 + line
    (1..em_count).each { line += "</em>" }

    # 引用
    if line[0, 4] == '&gt;'
      print "<span class=\"msg-quote\">#{line}</span><br>\n"
    else
      print line, "<br>\n"
    end
  }
end


def get_entry entry_id
  entry = MDB.get entry_id

  # 後方互換性
  if entry.charset && entry.charset.downcase == "euc-jp"
    t = Message.new
    t.head = entry.head
    t.body = (entry.body.map do |x| e2u(x) end)
    return t
  end

  return entry
end


# エントリ一つを表示
def board_msg_1_put article, first, last
  fname = mesno(article)
  begin 
    msg = get_entry fname
  rescue SystemCallError
    printf strgets(4), fname, fname
    return 0
  end

  # 日付
  date = get_time(msg.head['date'])
  if date
    date_str = date.localtime.strftime("%Y.%m.%d %H:%M")
  else
    date_str = CGI.escapeHTML(msg.head['date']||"") # for backward compatibility
  end
  print <<EOF
<div class="msg-header">
  &nbsp; <a name="#{fname}">[#{fname}]</a>
  <b>#{CGI.escapeHTML(msg.head['from']||"nil")}</b> (#{date_str})
EOF
  printf "<a href=\"./index.rb?action=reply&amp;article=%s\">%s</a>", fname, strgets(5)
  print <<EOF
  <input type="checkbox" name="del" value="#{fname}">
</div>
EOF
  if msg.head['cc']
    print "Cc: ", CGI.escapeHTML(msg.head['cc']), "<br>\n"
  end

  if (re_num = msg.get_re) != nil
    have_re = false
    re_num.each { |re|
      begin
        mes_ = get_entry re
        have_re = true
        if re.to_i >= first && re.to_i <= last
          print "<a href=\"##{mesno(re)}\">#{mesno(re)} #{mes_.head['from']}</a> "
        else
          print "<a href=\"./index.rb?action=follow&amp;article=#{mesno(re)}##{mesno(re)}\">#{mesno(re)} #{mes_.head['from']}</a> "
        end
      rescue SystemCallError
      end
    }
    if have_re == true
      print strgets(6)
    end
  end

  if msg.head["content-type"].split(";")[0].strip == "text/html"
    html_formatter(msg)
  else
    plain_formatter(msg)
  end

  # 添付ファイル
  if msg.head['attach']
    orig_name = CGI::escapeHTML(msg.head['attach'])
    if msg.head['attach-file']
      a_fname = File.basename(msg.head['attach-file'])
    else
      a_fname = fname + ".attach"
    end
    a_fname.untaint
    if FileTest.size?(BOARD_ATTACH_DIR + "/" + a_fname) != nil
      ext = get_ext_name(BOARD_ATTACH_DIR + "/" + a_fname).downcase
      if ext == "jpg" || ext == "gif" || ext == "png" || ext == "jpeg"
        printf "<img src=\"%s/%s\">", BOARD_ATTACH_PUBLIC_DIR, a_fname
      else
        printf strgets(7), msg.mes_id, orig_name,
                  decimal_format(FileTest.size?(BOARD_ATTACH_DIR + "/" + a_fname))
      end
    end
  end

  # フォローアップ
  fol_fname = PRIV_DIR + "/data/" + fname + ".follow"
  if FileTest.file?(fol_fname)
    fol_fp = File.open(fol_fname, "r")
    have_fol = false
    while fol = fol_fp.gets
      begin
        mes_ = get_entry fol
        if have_fol == false
          have_fol = true
          print "<div style=\"margin-top:0\" class=\"follow\">follow-up(s): "
        end
        follow = fol.to_i
        if follow >= first && follow <= last 
          print "<a href=\"##{mesno(follow)}\">#{mesno(follow)} #{mes_.head['from']}</a> "
        else
          print "<a href=\"./index.rb?article=#{mesno(follow)}##{mesno(follow)}\">#{mesno(follow)} #{mes_.head['from']}</a> "
        end
      rescue SystemCallError
      end
    end
    print "</div>" if have_fol
    print "\n"
    fol_fp.close
  end

  if msg.head['delivered']
    print "(このメッセージはメール配信されました)\n" # for backward compatibility
  end
  return 1
end


def board_msg_footer()
  print <<EOF
<table width="100%" style="margin-top:1em">
<tr><td>
<table align="right" class="form">
#{strgets(8)}
</table>
</table>
</form>
EOF
end


# 入力ペイン
def board_post_pane cgi, user, number, reply = nil
  raise TypeError if !user.is_a?(String) && !user.is_a?(NilClass)
  raise TypeError if !number.is_a?(Integer)
  raise TypeError if !reply.is_a?(String) && !reply.is_a?(NilClass)

  tmpl_data = {
    "page-size" => BOARD_PAGE_SIZE
  }
  fil_data = {
    "number" => number
  }

  message = get_flash cgi, "alert"
  if message
    tmpl_data["if-alert"] = {"message" => message}
  end

  # ユーザー
  if user && user != ""
    tmpl_data["if-auth"] = {"name" => CGI.escapeHTML(user)}
    fil_data["user"] = user
  else
    tmpl_data["if-no-auth"] = {}
  end

  # 返信・引用
  if reply
    begin
      t = ""
      mes = get_entry reply
      if mes.head["content-type"].split(/\;[ ]*/, 2)[0] == "text/html"
        HTMLSplit.new(mes.body.join("\n")).document.each {|e|
          t += e.to_s if e.is_a?(CharacterData)
        }
        t = t.gsub("\n\n", "\n").split("\n").map! {|line| "> " + line}.join("\n") + "\n"
      else
        mes.body.each {|line|
          if line != ""
            t += "> " + line.gsub(/<em[ \t]*>/i, '').gsub(/<\/em[ \t]*>/i, '') + "\n"
          end
        }
      end

      fil_data.update({
        "re" => reply,
        "body" => t
      })
    rescue SystemCallError
    end
  end

  # 配信
  deliver = []
  StrStore.new(PRIV_DIR + "/kname").transaction_ro {|profdb|
    profdb.keys.each {|k|
      prof = Profile.parse(k, profdb[k])
      if prof.mob_mail && prof.mob_mail != ""
        deliver << CGI.escapeHTML(prof.name) +
                      "&lt;" + CGI.escapeHTML(prof.mob_mail) + "&gt;"
      end
    }
  }
  tmpl_data["deliv"] = deliver.join(", ")

  # 次ページ・前ページ
  if number > 1
    tmpl_data["prev-link"] = "<a href=\"./index.rb?action=prev&amp;cur=#{number}\">" + 
                             sprintf(strgets(14), BOARD_PAGE_SIZE) + "</a>"
  else
    tmpl_data["prev-link"] = sprintf(strgets(14), BOARD_PAGE_SIZE)
  end

  if number + BOARD_PAGE_SIZE <= get_counter(PRIV_DIR + "/counter")
    tmpl_data["next-link"] = "<a href=\"./index.rb?action=next&amp;cur=#{number}\">" +
                             sprintf(strgets(15), BOARD_PAGE_SIZE) + "</a>"
  else
    tmpl_data["next-link"] = sprintf(strgets(15), BOARD_PAGE_SIZE)
  end

  tmpl = HTML::Template.new APP_ROOT + "/templates/form-ja.html"
  fil = HTML::FillInForm.new(tmpl.expand(tmpl_data).output)
  return fil.fill(fil_data)
end


def error_bt_out()
  print "Content-Type: text/html\n\n"
  print "<p>#{CGI.escapeHTML($!.inspect)}<br>\n"
  $@.each {|x| print CGI.escapeHTML(x), "<br>\n"}
end


# アクセスログに追記する.
def access_log userid
  now = Time.now

  File.open(PRIV_DIR + "/access.log", "a") do |fp|
    fp.flock(File::LOCK_EX)

    fp.print [ (ENV["HTTP_X_FORWARDED_FOR"] || ENV['REMOTE_ADDR'] || "-"),
      (ENV["REMOTE_USER"] || "-"),
      now.iso8601,
      ENV["REQUEST_METHOD"],
      ENV["REQUEST_URI"],
      (userid || "-") ].join("\t"), "\n"
  end
end
