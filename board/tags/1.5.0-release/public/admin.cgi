#!/home/pak04955/bin/ruby
# -*- mode: ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www2.airnet.ne.jp/pak04955/

require "ftools.rb"
require "board-sub.rb"
require "board-conf.rb"
require "common/message.rb"

$SAFE = 1

def check_pwd(pwd)
  File.open(PWD_FILE, "r") {|fp|
    return pwd == fp.gets.gsub(/[\r\n]/, '')
  }
end

def del_articles(cgi)
  ary = cgi["del"]
  ary.each {|mesno_|
    File.move(BOARD_DATA_DIR + "/" + mesno(mesno_), BOARD_DEL_DIR + "/" + mesno(mesno_))
  }
  return ary
end

def result_out(ary)
  html_header_out("deleted")
  print strgets(1)
  ary.each {|mesno_|
    print "<li>", mesno(mesno_), "\n"
  }
  print "</ul>\n"
  html_footer_out()
end

# main
begin
  cgi = CGI.new
  if !check_pwd(cgi["pwd"].first)
    html_header_out("error")
    print strgets(2)
    html_footer_out()
    exit
  end
  ary = del_articles(cgi)
  result_out(ary)
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end
