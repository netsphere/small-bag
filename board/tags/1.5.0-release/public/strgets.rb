
def strgets_ja(id)
  case id
  when 1
    return <<EOF
<p>次の投稿を削除しました。
<ul>
EOF

  when 2
    return "<p>パスワードが違います。"

  when 3
    return <<EOF
Content-Type: text/html; charset=EUC-JP
Cache-Control: no-cache
Pragma: no-cache

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
  <LINK rel="start" href="http://www2.airnet.ne.jp/pak04955/">
  <LINK rel="copyright" href="http://www2.airnet.ne.jp/pak04955/copyright.htm">
  <LINK rel="stylesheet" href="./default.css" type="text/css">
  <META name="ROBOTS" content="NOINDEX, NOFOLLOW">
  <title>%s</title>
</head>
<body>
EOF

  when 4
    return <<EOF
<div class="msg-header">
  &nbsp; <a name="%s">[%s]</a>
</div>
(削除)
EOF

  when 5
    return <<EOF
<a href="./board.cgi?action=reply&amp;article=%s">[返信]</a>
EOF

  when 6
    return <<EOF
<span class="reply">への返信</span><br>
EOF

  when 7
    return <<EOF
<span class="attach">添付ファイル：</span>
<a href="./board.cgi?action=file&amp;article=%s" target="attach">%s</a> (%sバイト)<br>
EOF

  when 8
    return <<EOF
<table width="100%">
<tr><td>
<table align="right" class="form">
  <tr><th align="right">管理者パスワード：
    <td>
      <input type="password" name="pwd"> &nbsp;
      <input type="submit" value="削除">
</table>
</table>
</form>
EOF

  when 9
    return <<EOF
<form method="post" enctype="multipart/form-data" action="./board.cgi">
  <input type="hidden" name="action" value="post">
<table class="form">
  <tr><th nowrap align="right">名前：<td>
EOF

  when 10
    return <<EOF
<tr><th align="right">返信：
  <td><input type="text" name="re" size="20" value="%s">
<tr><th align="right" valign="top" nowrap>本文：
EOF

  when 11
    return <<EOF
<tr><th align="right">添付：
  <td><input type="file" name="file" size="30">
EOF

  when 12
    return <<EOF
<tr>
  <th align="right">配信：
    <td><input type="checkbox" name="deliver">メール配信する: 
EOF

  when 13
    return <<EOF
<tr><td colspan="2" align="center">
  <hr>
  <input type="submit" name="submit" value="投稿/更新"> &nbsp;
  <input type="reset" value="リセット"> ||
EOF

  when 14
    return "[前の %s 件]"

  when 15
    return "[次の %s 件]"

  when 16
    return <<EOF
  <a href="./board.cgi">[最新の %s 件]</a>
</table>
  <input type="hidden" name="number" value="%s">
</form>
<ul>
  <li>返信欄には複数の投稿番号を空白で区切って書けます。
  <li>引用は行頭で'&gt;'（半角大なり）。「返信」リンクをクリックすると，自動的に付加されます。
  <li>[投稿/更新]ボタンを連続して押さないでください。。特に添付ファイルがある時は反応がかなり遅くなります。
</ul>
EOF

  when 17
    return <<EOF
<form method="post" action="./search.cgi">
  <table class="form">
    <tr><th>全文検索：
      <td><input type=text name="text"> <input type="submit" value="検索">
  </table>
</form>
EOF

  when 18
    return <<EOF
Subject: =?ISO-2022-JP?B?%s?=
Content-Type: text/plain; charset=ISO-2022-JP
EOF

  when 19
    return "<p>error:「名前」欄が未入力。"

  when 20
    return "text/plain; charset=euc-jp"

  when 21
    return <<EOF
<h1>検索結果</h1>
<hr>
EOF

  when 22
    return "<p><a href=\"./board.cgi\">[戻る]</a>\n"

  end
end

def strgets_en(id)
  case id
  when 1
    s = <<EOF
<p>The following post(s) was deleted.
<ul>
EOF
    return s

  when 2
    return "<p>Incorrect password."

  when 3
    s = <<EOF
Content-Type: text/html; charset=iso-8859-1
Cache-Control: no-cache
Pragma: no-cache

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <LINK rel="start" href="http://www2.airnet.ne.jp/pak04955/">
  <LINK rel="copyright" href="http://www2.airnet.ne.jp/pak04955/copyright.htm">
  <LINK rel="stylesheet" href="./default.css" type="text/css">
  <META name="ROBOTS" content="NOINDEX, NOFOLLOW">
  <title>%s</title>
</head>
<body>
EOF
    return s

  when 4
    return <<EOF
<div class="msg-header">
  &nbsp; <a name="%s">[%s]</a>
</div>
(Deleted)
EOF

  when 5
    return <<EOF
<a href="./board.cgi?action=reply&amp;article=%s">[Reply]</a>
EOF

  when 6
    return <<EOF
<span class="reply"> Reply</span><br>
EOF

  when 7
    return <<EOF
<span class="attach">Attach: </span>
<a href="./board.cgi?action=file&amp;article=%s" target="attach">%s</a> (%s bytes)<br>
EOF

  when 8
    return <<EOF
<table width="100%">
<tr><td>
<table align="right" class="form">
  <tr><th align="right">Admin password:
    <td>
      <input type="password" name="pwd"> &nbsp;
      <input type="submit" value="Delete">
</table>
</table>
</form>
EOF

  when 9
    return <<EOF
<form method="post" enctype="multipart/form-data" action="./board.cgi">
  <input type="hidden" name="action" value="post">
<table class="form">
  <tr><th nowrap align="right">Name:<td>
EOF

  when 10
    return <<EOF
<tr><th align="right">Contrib#:
  <td><input type="text" name="re" size="20" value="%s">
<tr><th align="right" valign="top" nowrap>Text:
EOF

  when 11
    return <<EOF
<tr><th align="right">Attached file:
  <td><input type="file" name="file" size="30">
EOF

  when 12
    return <<EOF
<tr>
  <th align="right">Delivery:
    <td><input type="checkbox" name="deliver">Deliver email to:
EOF

  when 13
    return <<EOF
<tr><td colspan="2" align="center">
  <hr>
  <input type="submit" name="submit" value="Post"> &nbsp;
  <input type="reset" value="Clear"> ||
EOF

  when 14
    return "[Previous %s posts]"

  when 15
    return "[Next %s posts]"

  when 16
    return <<EOF
  <a href="./board.cgi">[The latest %s posts]</a>
</table>
  <input type="hidden" name="number" value="%s">
</form>
<ul>
  <li>You can safely ignore the Contrib# field, but you can write plural, space delimited, post numbers in the field.
  <li>As for quotation, use the '&gt;'. (it's added automatically when you click the [Reply] link.)
  <li>Be patient when pushing the [Post] button. It slow, specially when you've attached a file.
</ul>
EOF

  when 17
    return <<EOF
<form method="post" action="./search.cgi">
  <table class="form">
    <tr><th>Search:
      <td><input type=text name="text"> <input type="submit" value="Search">
  </table>
</form>
EOF

  when 18
    return <<EOF
Subject: =?ISO-8959-1?B?%s?=
Content-Type: text/plain; charset=iso-8859-1
EOF

  when 19
    return "<p>error: Unknown user."

  when 20
    return "text/plain; charset=iso-8859-1"
    
  when 21
    return <<EOF
<h1>Search result</h1>
<hr>
EOF

  when 22
    return "<p><a href=\"./board.cgi\">[Back]</a>\n"

  end
end

def strgets(id)
  lc = $LC_MESSAGES || $LANG
  if lc == "en"
    strgets_en(id)
  else
    strgets_ja(id)
  end
end
