#!/home/pak04955/bin/ruby
# -*- Mode: ruby -*-

require 'cgi'
require 'board-sub'

$SAFE = 1
ENV['PATH'] = '/bin'

  html_header_out("Q's WebBoard")

  counter = get_counter(BOARD_COUNTER_FILE)
  
  cgi = CGI.new
  user = if !cgi['user'].empty? then cgi['user'][0].read else "" end
  action = cgi['action'].first
  article = if !cgi['article'].empty? then cgi['article'][0].to_i else 0 end
  number = if !cgi['number'].empty? then cgi['number'][0].read.to_i else 0 end
  board_post_pane(user, counter, number, action, article)
