#!/home/pak04955/bin/ruby
# -*- mode:ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www2.airnet.ne.jp/pak04955/

require 'cgi'
# require 'dbm'
#   DBMはAIXで正常に動作せず。

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end
require File.rel2abs("./board-sub.rb", __FILE__)
require File.rel2abs("./common/common.rb", __FILE__)
require File.rel2abs("./common/injector.rb", __FILE__)
require File.rel2abs("./common/message.rb", __FILE__)

$SAFE = 1
ENV['PATH'] = '/bin'

$mdb = MessageSpace.new(BOARD_DATA_DIR)

class CGI
  def get(name)
    if self[name].empty?
      nil
    elsif self[name].first.instance_of?(String)
      self[name].first
    else
      self[name].first.read
    end
  end
end

def get_ext_name(pathname, ext = nil)
  # 戻り値
  # 拡張子　ピリオドは含まない
  base = File.basename(pathname)
  r = base.rindex('.')
  if r == nil || r == 0
    return ext
  else
    return base[r + 1, base.length]
  end
end

def view_out(user, page_start, reply = nil)
  if !page_start
    page_start = [get_counter(BOARD_COUNTER_FILE) - BOARD_PAGE_SIZE + 1, 1].max
  end

  html_header_out(BBS_TITLE)

  # 投稿フォームを表示
  board_post_pane(user, page_start, reply)

  # 検索フォーム
  print strgets(17)

  # 発言
  last = [page_start + BOARD_PAGE_SIZE - 1, get_counter(BOARD_COUNTER_FILE)].min
  if USE_THREAD != false
    board_thread(page_start, last)
  else
    board_msg_put(page_start, last)
  end

  # フッター
  html_footer_out()
  exit
end

def attach_file_out(article)
  msg = $mdb.get(article)
  if msg.head['attach-file']
    vafname = "/" + File.basename(msg.head['attach-file'])
  else
    vafname = "/" + mesno(article) + ".attach"
  end
  vafname.untaint
  attach_length = FileTest.size?(BOARD_ATTACH_DIR + vafname)
  if attach_length != nil
    extname = get_ext_name(BOARD_ATTACH_DIR + vafname)
    if extname == "rb" || extname == "pl" || extname == "cgi"
      print "Content-Type: application/octet-stream\n"
      print "Content-Length: #{attach_length}\n"
      print "\n"

      fp = File.open(BOARD_ATTACH_DIR + vafname, "r")
      fp.binmode
      $stdout.binmode
      $stdout.write(fp.read)
    else
      # content-typeはWebサーバーに決めさせる
      print "Status: ", (ENV['SERVER_PROTOCOL'] == "HTTP/1.1" ? "303" : "302"), "\n"
      print "Location: " + BOARD_ATTACH_PUBLIC_DIR + vafname + "\n"
      print "\n"
    end
  else
    print "Status: 404 Not Found\n"
    print "\n"
  end
  exit
end

def deliver(mes, toaddr)
  # メール配信する
  s = sprintf(strgets(18), [e2j(DELIVER_SUBJECT)].pack("m").chomp)
  s += <<EOF
From: #{DELIVER_FROM}
To: #{toaddr.join(',')}
MIME-Version: 1.0
Errors-To: #{ADMIN_MAIL}
Reply-To: #{ADMIN_MAIL}
EOF
  s += "\n"
  s += mes.body.join("\n")
  send_mail(e2j(s))
end

def access_log(cgi, user)
  # ログに追記する
  File.open(ACCESS_LOG, "a") {|fp|
    fp.flock(File::LOCK_EX)
    host = ENV['REMOTE_HOST'] || ENV['REMOTE_ADDR']
    fp.print "#{CGI.rfc1123_date(Time.now)}\t#{host}\t#{user}\n"
  }
end

=begin
2000.07.02 Webブラウザによっては子フレームのアクセスでHTTP_REFERERを投げない
  if !ENV['HTTP_REFERER'] || ENV['HTTP_REFERER'].index(BOARD_URI) != 0
    # 外からのアクセスのとき
    print "<p>投稿は #{BOARD_URI} からしかできません\n"
    html_footer_out()
    exit
  end
=end

def post(cgi, user)
  re_num = cgi.get("re").split(/[ \t]+/)
  body = cgi.get("body").strip
  attach = cgi['file']

  if !user || user == ""
    html_header_out(BBS_TITLE)
    print strgets(19)
    html_footer_out
    exit
  end

  if body == ""
    view_out(user, nil)
    exit
  end

  # 改行は削除しておく
  user.gsub! /[\r\n]/, ''

  # 同一内容？
  check = File.open(BOARD_CHECK_FILE, File::RDWR | File::CREAT)
  check.flock(File::LOCK_EX)
  ch_data = check.readlines
  view_out(user, nil) if body == ch_data.join.strip
  check.reopen(BOARD_CHECK_FILE, "w+")
  check.puts(body)
  check.close

  now = Time.now
  counter = incr_counter(BOARD_COUNTER_FILE)

  # 添付ファイルを生成する
  have_attach = false
  if !attach.empty?
    st = attach[0].stat
    if st.size > 0 && st.size < 1000000
      have_attach = true
      extname = "." + get_ext_name(attach[0].original_filename.gsub(/\\/, '/'), "attach")

      a_fname = BOARD_ATTACH_DIR + "/" + mesno(counter) + extname
      a_fname.untaint
      File.open(a_fname, "w") {|a_file|
        attach[0].rewind # .sizeでストリームの末尾に移動している
        a_file.write(attach[0].read)
      }
    end
  end

  # 発言を生成する
  new_mes = Message.new
  new_mes.head['from'] = user
  new_mes.head['date'] = CGI.rfc1123_date(now)

  # 返信
  if re_num && re_num.size > 0
    new_mes.head['in-reply-to'] = ""
    re_num.each {|re|
      if re.to_i > 0
        begin
          $mdb.get(re) # 本当に返信先がある？
        rescue SystemCallError
          # 返信先がないときは単に無視
          next
        end

        # 返信先に返信元を付ける
        fol_fname = BOARD_DATA_DIR + "/" + mesno(re) + ".follow"
        File.open(fol_fname, "a") {|fol_file|
          fol_file.flock(File::LOCK_EX)
          fol_file.print "#{counter}\n"
        }

        new_mes.head['in-reply-to'] << " #{re}"
      end
    }
  end

  new_mes.head['content-type'] = strgets(20)

  if have_attach != false
    new_mes.head['attach'] = File.basename(attach[0].original_filename.gsub(/\\/, '/'))
    new_mes.head['attach-file'] = "#{mesno(counter)}#{extname}"
  end

  new_mes.body = body.split(/\r?\n/)

  if !cgi['deliver'].empty?
    toaddr = Array.new
    File.open(DELIVER_MAIL, "r") {|fp|
      while line = fp.gets
        toaddr << line.gsub(/[\r\n]/, '')
      end
    }
    new_mes.head['cc'] = toaddr.join(',')
    deliver(new_mes, toaddr)
  end
  $mdb.write(new_mes, counter)

  # 表示を更新する
  view_out(user, nil)
end

def get_user(cgi)
  get_kname(ENV['REMOTE_USER']) || cgi.get("user")
end

begin
  cgi = CGI.new
  user = get_user(cgi)
  access_log(cgi, user)
  case cgi.get("action")
  when "file"
    # 添付ファイルを見る
    article = cgi.get("article")
    attach_file_out(article)
  when "post"
    # 投稿
    post(cgi, user)
  when "next"
    page_start = [cgi.get("cur").to_i + BOARD_PAGE_SIZE,
                  get_counter(BOARD_COUNTER_FILE)].min
    view_out(user, page_start)
  when "prev"
    page_start = [cgi.get("cur").to_i - BOARD_PAGE_SIZE, 1].max
    view_out(user, page_start)
  when "reply"
    # 返信
    article = cgi.get("article")
    view_out(user, nil, article)
  when "follow"
    article = cgi.get("article").to_i
    page_start = [article - BOARD_PAGE_SIZE / 2, 1].max
    view_out(user, page_start)
  else
    view_out(user, nil)
  end
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end
