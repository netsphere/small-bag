
# -*- encoding:euc-jp -*-

# Copyright (c) 2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www2.airnet.ne.jp/pak04955/

class StrStore
  def initialize(file)
    dir = File.dirname(file)
    if !FileTest.directory?(dir)
      raise ArgumentError, sprintf("directory %s does not exist", dir)
    end
    if FileTest.exist?(file)
      if !FileTest.readable?(file)
        raise ArgumentError, sprintf("file %s not readable", file)
      end
    else
      if !FileTest.writable?(dir)
        raise ArgumentError, sprintf("directory %s not writable", dir)
      end
    end
    @transaction = false
    @path = file
    @abort = false
    @rdonly = false
    @table = nil
  end
  attr_reader :path

  def [](key)
    key_check(key)
    in_transaction
    raise IndexError, sprintf("key %s undefined", key) if !@table.key?(key)
    @table[key]
  end

  def []=(key, value)
    key_check(key)
    raise TypeError, sprintf("value %s not string", value) if !value.kind_of?(String)
    raise ArgumentError, sprintf("value %s includes CR or LF", value) if value =~ /[\r\n]/
    in_transaction_wr
    @table[key] = value
  end

  def delete(key)
    key_check(key)
    in_transaction_wr
    @table.delete(key)
  end

  def clear
    in_transaction_wr
    @table.clear
  end

  def keys
    in_transaction
    @table.keys
  end

  def key?(key)
    key_check(key)
    in_transaction
    @table.key?(key)
  end

  def to_a
    in_transaction
    @table.to_a
  end

  def commit
    in_transaction
    @abort = false
    throw :inistore_abort_transaction
  end

  def abort
    in_transaction
    @abort = true
    throw :inistore_abort_transaction
  end

  def transaction
    raise "nested transaction" if @transaction
    begin
      @rdonly = false
      @abort = false
      @table = {}
      @transaction = true
      value = nil
      fp = File.open(@path, File::RDWR | File::CREAT)
      fp.flock(File::LOCK_EX)
      fp.each_line {|line|
        k, v = line.gsub(/[\r\n]/, '').split('=', 2)
        @table[k] = v
      }
      begin
        catch (:inistore_abort_transaction) do
          value = yield
        end
      rescue Exception
        @abort = true
        raise
      ensure
        if !@abort
          fp.rewind
          fp.truncate(0)
          @table.each {|k, v|
            fp.print "#{k}=#{v}\n"
          }
        end
      end
    ensure
      @transaction = false
      @table = nil
      fp.close if fp
    end
  end

  def transaction_ro
    raise "nested transaction" if @transaction
    begin
      @rdonly = true
      @abort = false
      @table = {}
      @transaction = true
      value = nil
      fp = File.open(@path, File::RDONLY | File::CREAT)
      fp.flock(File::LOCK_SH)
      fp.each_line {|line|
        k, v = line.gsub(/[\r\n]/, '').split('=', 2)
        @table[k] = v
      }
      catch (:inistore_abort_transaction) do
        value = yield
      end
    ensure
      @transaction = false
      @table = nil
      fp.close if fp
    end
    value
  end

  private
  def in_transaction
    raise "not in transaction" if !@transaction
  end

  def in_transaction_wr
    in_transaction
    raise "in read-only transaction" if @rdonly
  end
  
  def key_check(key)
    raise TypeError, sprintf("key %s not string", key) if !key.kind_of?(String)
    raise ArgumentError, sprintf("key %s includes '=' or space", key) if key =~ /[= \t\r\n]/
  end
end
