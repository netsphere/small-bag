
# -*- mode:ruby; encoding:euc-jp -*-

require 'nkf'

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end
require File.rel2abs("injector-conf.rb", __FILE__)

def e2j(s) # eucJP -> iso-2022-jp
  s ? NKF.nkf("-j -E -m0", s) : nil
end

def j2e(s)
  s ? NKF.nkf('-e -J -m0', s) : nil
end

class Mail
  alias :orig_initialize :initialize

  def initialize(io = nil)
    if io
      orig_initialize(io)
    else
      @header = {}
      @body = []
    end
  end

  def header=(hash)
    @header = hash
  end

  def body=(ary)
    @body = ary
  end
end

class String
  def shift_e
    return nil if self == ""
    a = split(//e, 2)
    replace(a[1])
    a[0]
  end

  def unshift(s)
    self[0, 0] = s
  end
end

# メールヘッダーを生成する
# @param name ヘッダーフィールド名
# @param value ヘッダーフィールド値（日本語EUC）
def mail_header(name, value)
  return name + ": " + value + "\n" if value !~ /[^\x1-\x7f]/
  result = name + ":"
  str = value.dup
  while str.length > 0
    line = ""
    while (c = str.shift_e) != nil && 
          e2j(line + c).to_a.pack("m").chomp.length < 75 - 18
      line += c
    end
    str.unshift(c) if c
    result += " =?ISO-2022-JP?B?" + 
              e2j(line).to_a.pack("m").chomp + "?=\n"
  end
  return result
end

def send_mail(s)
  # メールを送信する
  begin
    pipe = IO.popen("#{INJECTOR}", "w")
    s.split(/\r?\n/).each {|line|
      pipe.print line, "\n"
    }
  ensure
    pipe.close if pipe
  end
end
