
# -*- Mode: ruby; encoding: euc-jp -*-

# ruby library
# Copyright (c) 1999-2000 HORIKAWA Hisashi. All rights reserved.
#     mailto:vzw00011@nifty.ne.jp
#     http://www2.airnet.ne.jp/pak04955/

# 細々とした関数群

# 3桁ごとに','を入れる
def decimal_format(s)
  return nil if !s
  if s >= 0
    str = s.to_s.reverse
    return str.scan(/.?.?./).join(',').reverse
  else
    str = (-s).to_s.reverse
    return "-" + str.scan(/.?.?./).join(',').reverse
  end
end

## 文字列をTimeオブジェクトに変換する
# @param s 時刻を表す文字列
#   RFC 2616
#      Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
#      Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
#      Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
def get_time(s)
  return nil if !s
     
  d = s.split(/[, :-]+/)
  if CGI::RFC822_MONTHS.include?(d[2]) && d.size == 8
    # RFC 822
    return Time.gm(d[3], CGI::RFC822_MONTHS.index(d[2]) + 1, d[1], 
                   d[4], d[5], d[6])
  elsif Date::DAYNAMES.include?(d[0]) && d.size == 8
    # RFC 850
    return Time.gm(1900 + d[3], CGI::RFC822_MONTHS.index(d[2]) + 1, d[1],
                   d[4], d[5], d[6])
  elsif CGI::RFC822_MONTHS.include?(d[1]) && d.size == 7
    # asctime
    return Time.gm(d[6], CGI::RFC822_MONTHS.index(d[1]) + 1, d[2],
                   d[3], d[4], d[5])
  end
  return nil
end

def get_counter(fname)
  File.open(fname, "r") {|count_f|
    count_f.flock(File::LOCK_SH)  
    return count_f.gets.to_i
  }
end

# カウンタを加算し，（加算後の）新しい番号を返す
def incr_counter(fname, mode = File::RDWR | File::CREAT)
  cnt = nil
  File.open(fname, mode) {|fp|
    fp.flock(File::LOCK_EX)
    cnt = fp.gets.to_i + 1
    fp.rewind
    fp.truncate(0)
    fp.puts(cnt)
  }
  return cnt
end

def get_attrs(s)
  # XMLのタグの属性を取り出す
  # s = "attr="val" attr="val">" # 要素名は含まない
  r = Hash.new
  while s =~ /([a-z][a-z0-9]*)[ \t\r\n]*=[ \t\r\n]*\"([^\"]*)\"/
    attr_name = $1
    attr_value = $2.strip
    s = $'
    r[attr_name] = attr_value
  end
  r
end

