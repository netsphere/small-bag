
# -*- mode:ruby; encoding:euc-jp -*-

# メール送信コマンド
# 標準入力からメールを読み込んで送信するもの
# 必ずフルパスで設定すること
INJECTOR = '/var/qmail/bin/qmail-inject'
#INJECTOR = "/usr/sbin/sendmail -i -t"

# sendmailオプション
#     -i '.'行を特別扱いしない。
#     -t 送信先をコマンドラインではなく，メッセージヘッダーで指定する
