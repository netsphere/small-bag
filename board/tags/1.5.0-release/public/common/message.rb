
# -*- encoding:euc-jp -*-

# メールメッセージに似たデータ形式

def mesno(i)
  if i.to_i < 10000
    sprintf("%04d", i.to_i)
  else
    sprintf("%d", i.to_i)
  end
end

class Message
  def initialize()
    @head = Hash.new
    @body = Array.new
  end
  attr :head, true
  attr :body, true

  def get_re()
    # RFC 2076 In-Reply-To:   RFC 822: 4.6.2.
    re_num = @head['re'].split(/[ \t]+/) if @head['re'] # 互換性
    re_num = @head['in-reply-to'].split(/[ \t]+/) if @head['in-reply-to']
    return re_num && re_num.size > 0 ? re_num : nil
  end

  def mes_type()
    return @head['type']
  end

  def mes_type=(t)
    @head['type'] = t
  end

  def mes_id()
    return mesno(@head['article'])
  end

  def mes_id=(a)
    @head['article'] = mesno(a)
  end
end # of class Message

class MessageSpace
  def initialize(d)
    @loc = d
    @cache = Hash.new
  end

  def get(article)
    if @cache[mesno(article)]
      mes = Message.new
      mes.head = @cache[mesno(article)].head
      mes.body = @cache[mesno(article)].body
      return mes
    end

    begin
      fp = File.open(@loc + "/" + mesno(article), "r")
      fp.flock(File::LOCK_SH)

      mes = Message.new

      # http://www.ietf.org/internet-drafts/draft-ietf-drums-msg-fmt-09.txt

      # ヘッダ
      while line = fp.gets
        line.gsub! /[\r\n]/, ''
        break if line == "" # 3.5 Overall message syntax
        if line =~ /^([\x21-\x39\x3b-\x7e]+):\s*(.*)/
          n = $1.downcase
          b = $2.strip
          if mes.head[n]
            mes.head[n] << " #{b}"
          else
            mes.head[n] = b
          end
          last_field = n
        elsif line =~ /^[ \t]/ && last_field
          mes.head[last_field] << " #{line.strip}"
        end
      end
      mes.head['article'] = mesno(article)

      # 本文
      while line = fp.gets
        mes.body << line.gsub(/[\r\n]/, '')
      end 
      @cache[mesno(article)] = mes
    ensure
      fp.close if fp
    end
  end

  def write(mes, article = nil)
    mes.head['article'] = mesno(article) if article
    article = mes.head['article'] if !article
    # dateは作成日
    mes.head['date'] = CGI.rfc1123_date(Time.now) if mes.head['date'] == nil

    fname = @loc + "/" + mesno(article)
    File.open(fname, File::WRONLY | File::CREAT) {|fp|
      fp.flock(File::LOCK_EX)
      fp.rewind
      fp.truncate(0)
      mes.head.each { |h, v|
        fp.print "#{h}: #{v}\n"
      }
      fp.print "\n"
      mes.body.each {|line|
        fp.print "#{line}\n"
      }
    }
    @cache[mesno(article)] = mes
  end
end # of class MessageSpace
