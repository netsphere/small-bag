
# -*- Mode: ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www2.airnet.ne.jp/pak04955/

require 'date'
require 'cgi'

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end
require File.rel2abs("./board-conf.rb", __FILE__)
require File.rel2abs("./strgets.rb", __FILE__)
require File.rel2abs("./common/common.rb", __FILE__)

def uri_re
  alphanum      = "[0-9A-Za-z]"
  escaped       = "(%[0-9A-Fa-f][0-9A-Fa-f])"
  mark          = "[-_.!~*'()]"
  unreserved    = "(?:#{alphanum}|#{mark})"
  reserved      = "[;/?:@&=+$,]"
  uric          = "(?:#{reserved}|#{unreserved}|#{escaped})"
  fragment      = "(#{uric}*)"
  query         = "(#{uric}*)"
  pchar         = "(?:#{unreserved}|#{escaped}|[:@&=+$,])"
  param         = "(#{pchar}*)"
  segment       = "(#{pchar}*(?:;#{param})*)"
  abs_path      = "((/#{segment})+)"
  uric_no_slash = "(?:#{unreserved}|#{escaped}|[;?:@&=+$,])"
  opaque_part   = "(#{uric_no_slash}(?:#{uric})*)"
  port          = "([0-9]+)"
  ipv4address   = "([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)"
  toplabel      = "([A-Za-z]([-0-9A-Za-z]*#{alphanum})?)"
  domainlabel   = "(#{alphanum}([0-9A-Za-z-]*#{alphanum})?)"
  hostname      = "((?:#{domainlabel}\\.)*#{toplabel})"
  host          = "(?:#{hostname}|#{ipv4address})"
  hostport      = "(#{host}(?::#{port})?)"
  userinfo      = "((?:#{unreserved}|#{escaped}|[;:&=+$,])+)"
  server        = "((?:#{userinfo}@)?#{hostport})"
  scheme        = "([A-Za-z][A-Za-z0-9+.-]*)"
  net_path      = "(//#{server}(?:#{abs_path})?)"
  hier_part     = "(#{net_path}(?:\\?#{query})?)"
  absoluteURI   = "(#{scheme}:(?:#{hier_part}|#{opaque_part}))"
  uri_reference = "#{absoluteURI}(?:##{fragment})?"
  /#{uri_reference}/
end

def html_header_out(title)
  printf strgets(3), title
end

def html_footer_out
  print <<EOF
<hr>
<p align=right>Powerd by #{APP_TITLE}.<br>Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved<a name="tail">.</a>
</body>
</html>
EOF
end

def board_msg_1_put(article, first, last)
  fname = mesno(article)
  begin 
    msg = $mdb.get(fname)
  rescue SystemCallError
    printf strgets(4), fname, fname
    return 0
  end

  # 日付
  date = get_time(msg.head['date'])
  if date
    date_str = date.localtime.strftime("%Y.%m.%d %H:%M")
  else
    date_str = CGI::escapeHTML(msg.head['date']) # for backward compatibility
  end
  print <<EOF
<div class="msg-header">
  &nbsp; <a name="#{fname}">[#{fname}]</a>
  <b>#{CGI.escapeHTML(msg.head['from'])}</b> (#{date_str})
EOF
  printf strgets(5), fname
  print <<EOF
  <input type="checkbox" name="del" value="#{fname}">
</div>
EOF
  if msg.head['cc']
    print "Cc: ", msg.head['cc'], "<br>\n"
  end

  if (re_num = msg.get_re) != nil
    have_re = false
    re_num.each { |re|
      begin
        mes_ = $mdb.get(re)
        have_re = true
        if re.to_i >= first && re.to_i <= last
          print "<a href=\"##{mesno(re)}\">#{mesno(re)} #{mes_.head['from']}</a> "
        else
          print "<a href=\"./board.cgi?action=follow&amp;article=#{mesno(re)}##{mesno(re)}\">#{mesno(re)} #{mes_.head['from']}</a> "
        end
      rescue SystemCallError
      end
    }
    if have_re == true
      print strgets(6)
    end
  end

  # 本文
  msg.body.each {|line|
    line.gsub!(/[\r\n]/, '')
    line2 = ""
    while line =~ uri_re
      line2 += CGI.escapeHTML($`)
      line2 += "<a href=\"#{CGI.escapeHTML($&)}\" target=\"_top\">#{CGI.escapeHTML($&)}</a>"
      line = $'
    end
    line = line2 + CGI.escapeHTML(line)

    # 引用
    if line[0, 4] == '&gt;'
      print "<span class=\"msg-quote\">#{line}</span><br>\n"
    else
      print line, "<br>\n"
    end
  }

  # 添付ファイル
  if msg.head['attach']
    orig_name = CGI::escapeHTML(msg.head['attach'])
    if msg.head['attach-file']
      a_fname = BOARD_ATTACH_DIR + "/" + File.basename(msg.head['attach-file'])
    else
      a_fname = BOARD_ATTACH_DIR + "/" + fname + ".attach"
    end
    a_fname.untaint
    if FileTest.size?(a_fname) != nil
      printf strgets(7), msg.mes_id, orig_name, decimal_format(FileTest.size?(a_fname))
    end
  end

  # フォローアップ
  fol_fname = BOARD_DATA_DIR + "/" + fname + ".follow"
  if FileTest.file?(fol_fname)
    fol_fp = File.open(fol_fname, "r")
    have_fol = false
    while fol = fol_fp.gets
      begin
        mes_ = $mdb.get(fol)
        if have_fol == false
          have_fol = true
          print "<span class=\"follow\">follow-up(s): "
        end
        follow = fol.to_i
        if follow >= first && follow <= last 
          print "<a href=\"##{mesno(follow)}\">#{mesno(follow)} #{mes_.head['from']}</a> "
        else
          print "<a href=\"./board.cgi?article=#{mesno(follow)}##{mesno(follow)}\">#{mesno(follow)} #{mes_.head['from']}</a> "
        end
      rescue SystemCallError
      end
    end
    print "</span>" if have_fol
    print "\n"
    fol_fp.close
  end

  if msg.head['delivered']
    print "(このメッセージはメール配信されました)\n" # for backward compatibility
  end
  return 1
end

def board_msg_footer()
  print strgets(8)
end

class Node
  def initialize(mesno)
    @me = mesno
    @parents = Array.new
    @children = Array.new
  end
  attr_accessor :me, :parents, :children
end

def board_thread_sub(already, node, first, last)
  if already[node.me]
    print <<EOF
<div class="msg-header">
&nbsp; <i><a href="##{mesno(node.me)}">[#{mesno(node.me)}] alias</a></i>
</div>
EOF
    return
  end
  if board_msg_1_put(node.me, first, last) > 0
    already[node.me] = true
    if node.children.size > 0
      print "<blockquote>\n"
      node.children.sort {|x, y| x.me <=> y.me}.each {|child|
        board_thread_sub(already, child, first, last)
      }
      print "</blockquote>\n"
    end
  end
end

# スレッド表示
def board_thread(first, last)
  print '<form method="post" action="./admin.cgi">', "\n"

  nodes = Array.new
  for i in 0 .. (last - first)
    nodes[i] = Node.new(first + i)
    begin
      mes = $mdb.get(first + i)
      if mes.get_re
        mes.get_re.each {|p|
          if p.to_i >= first && p.to_i < first + i
            nodes[i].parents << p.to_i
            nodes[p.to_i - first].children << nodes[i]
          end
        }
      end
    rescue SystemCallError
    end
  end
  already = {}
  (last - first).downto(0) {|i|
    if nodes[i].parents.size == 0
      board_thread_sub(already, nodes[i], first, last)
    end
  }
  board_msg_footer()
end

# リスト表示
def board_msg_put(first, last)
  print '<form method="post" action="./admin.cgi">', "\n"

  last.downto(first) {|i|
    board_msg_1_put(i, first, last)
  }
  board_msg_footer()
end

def get_kname(name)
  File.open(KNAME_FILE, "r") {|fp|
    while line = fp.gets
      line.gsub! /[\r\n]/, ''
      break if line == ""
      d = line.split(/[ \t]+/)
      return d[1] if d[0] == name
    end
  }
end

# 投稿フォームを表示
def board_post_pane(user, number, reply = nil)
  print strgets(9)

  if user && user != ""
    print user
    print '<input type="hidden" name="user" value="', user, "\">\n"
  else
    print '<input type="text" name="user" size="70">', "\n"
  end
  printf strgets(10), (reply || "")

  # 返信のとき，全文引用する
  if reply 
    print "  <td><textarea rows=6 cols=65 name=body>\n"
    fname = BOARD_DATA_DIR + "/" + mesno(reply)
    if FileTest.file?(fname) && FileTest.size?(fname)
      File.open(fname, "r") {|fp|
        while line = fp.gets
          break if line.gsub(/[\r\n]/, '') == ""
        end
        while line = fp.gets
          line.gsub! /[\r\n]/, ''
          if line != ""
            line = "&gt;" + CGI::escapeHTML(line)
            print line, "\n"
          end
        end
      }
    end
    print "</textarea>\n"
  else
    print "  <td><textarea rows=\"8\" cols=\"65\" name=\"body\"></textarea>\n"
  end

  print strgets(11)
  begin
    fp = File.open(DELIVER_MAIL, "r")
    print strgets(12)
    while line = fp.gets
      print line, " "
    end
  rescue SystemCallError
  ensure
    fp.close if fp
  end
  print strgets(13)

  if number > 1
    print "<a href=\"./board.cgi?action=prev&amp;cur=#{number}\">",
          sprintf(strgets(14), BOARD_PAGE_SIZE),
          "</a> &nbsp; "
  else
    print sprintf(strgets(14), BOARD_PAGE_SIZE), " &nbsp; "
  end

  if number + BOARD_PAGE_SIZE <= get_counter(BOARD_COUNTER_FILE)
    print "<a href=\"./board.cgi?action=next&amp;cur=#{number}\">",
          sprintf(strgets(15), BOARD_PAGE_SIZE),
          "</a> &nbsp; "
  else
    print sprintf(strgets(15), BOARD_PAGE_SIZE), " &nbsp; "
  end

  printf strgets(16), BOARD_PAGE_SIZE, number
end # of def

def error_bt_out()
  print "Content-Type: text/html\n\n"
  print "<p>#{CGI.escapeHTML($!.inspect)}<br>\n"
  $@.each {|x| print CGI.escapeHTML(x), "<br>\n"}
end
