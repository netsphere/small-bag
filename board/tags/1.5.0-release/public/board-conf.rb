
# -*- mode: ruby -*-
# 掲示板用設定ファイル

# Q's WebBoard
# Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www2.airnet.ne.jp/pak04955/

# title
BBS_TITLE = "My Private Board"

# message: "ja" or "en"
$LC_MESSAGES = "en"

# 掲示板の設置URI
# このURIの外からの投稿は全てキャンセルされる
BOARD_URI = "http://orange.fruits/~hori/board/"

# データファイルなどは，httpサーバーの認証を受けない場合，
# public_htmlの外に置くこと
PRIV_DIR = "/home/hori/wwwdata/board"

# 添付ファイルを置くディレクトリ ファイルの存在，大きさなどで用いる
BOARD_ATTACH_DIR = "/home/hori/public_html/board/attach"

# 添付ファイルがWeb経由でどのディレクトリとして見えるか。
# <a href="...">で用いる
BOARD_ATTACH_PUBLIC_DIR = "/~hori/board/attach"

# 一度に表示する発言数
BOARD_PAGE_SIZE = 20

# スレッド化するか  true / false
USE_THREAD = true

# 管理者のメールアドレス
ADMIN_MAIL = "vzw00011@nifty.ne.jp"

DELIVER_FROM = "hori@orange.fruits"
DELIVER_SUBJECT = "地下掲示板への投稿"

##########################################################################
# Do not edit.

# メール配信先メールアドレス一覧
DELIVER_MAIL = PRIV_DIR + "/deliver"

# パスワード・ファイルはpublic_htmlの外に置くこと
BOARD_DEL_DIR = PRIV_DIR + "/deleted"
PWD_FILE = PRIV_DIR + "/admin-pwd"

# アクセスログを記録するファイル
ACCESS_LOG = PRIV_DIR + "/access.log"

# パーソナルデータを保存するファイル
# 現状，ユーザー名 -> 長い名前の変換のみ
KNAME_FILE = PRIV_DIR + "/kname"

# 発言番号
BOARD_COUNTER_FILE = PRIV_DIR + "/counter"

# 多重発言をチェックするためのファイル
BOARD_CHECK_FILE = PRIV_DIR + "/check"

# 発言データを保存するディレクトリ
# 発言データは1発言1ファイルで保存する
BOARD_DATA_DIR = PRIV_DIR + "/data"

APP_TITLE = "Q's WebBoard"
