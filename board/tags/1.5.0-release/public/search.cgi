#!/home/pak04955/bin/ruby -Ke
# -*- Mode: ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www2.airnet.ne.jp/pak04955/

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end
require "cgi"
require File.rel2abs("./board-sub.rb", __FILE__)
require File.rel2abs("common/common.rb", __FILE__)

$SAFE = 1

# main

  if !ENV['HTTP_REFERER'] || ENV['HTTP_REFERER'].index(BOARD_URI) != 0
    # 外からのアクセスのとき
    print "Status: ", (ENV['SERVER_PROTOCOL'] == "HTTP/1.1" ? "303" : "302"), "\n"
    print "Location: #{ENV['HTTP_REFERER']}\n\n"
    exit
  end

  cgi = CGI.new
  text = cgi['text'].first || ""
  text.gsub! /[ 　\t\r\n]+/, ''
  if text == ""
    print "Status: ", (ENV['SERVER_PROTOCOL'] == "HTTP/1.1" ? "303" : "302"), "\n"
    print "Location: #{ENV['HTTP_REFERER']}\n\n"
    exit
  end

  html_header_out("search result")
  print strgets(21)

  counter = get_counter(BOARD_COUNTER_FILE)
  while counter >= 0
    fname = BOARD_DATA_DIR + "/" + sprintf("%04d", counter)
    begin
      if File.stat(fname).size <= 0
        counter -= 1
        next
      end
    rescue # ENOENT
      counter -= 1
      next 
    end

    fp = File.open(fname, "r")
    found = false

    # header
    while line = fp.gets
      break if line.gsub(/[\r\n]/, '') == ""
    end

    while line = fp.gets
      line.gsub! /[ 　\t\n\r]+/, ''
      if idx = line.upcase.index(text.upcase)
        if found == false
          printf "<p><a href=\"./board.cgi?action=follow&amp;article=%d#%s\">%s</a><br>\n", counter, sprintf("%04d", counter), sprintf("%04d", counter)
          found = true
        end
        print CGI::escapeHTML(line[0, idx]), "<font color=\"red\">",
        CGI::escapeHTML(line[idx, text.length]), "</font>",
        CGI::escapeHTML(line[idx + text.length, line.length]), "<br>\n"
      end
    end
    fp.close
    counter -= 1
  end

  print strgets(22)
  html_footer_out()
