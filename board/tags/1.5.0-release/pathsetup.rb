# Q's WebBoard
# Copyright (c) 1999-2001 HORIKAWA Hisashi. All rights reserved.
#     mailto:vzw00011@nifty.ne.jp
#     http://www2.airnet.ne.jp/pak04955/

# rubyパスを書き換える

require 'ftools.rb'
require 'tempfile.rb'

FILES = [
  'public/admin.cgi',
  'public/board.cgi',
  'public/search.cgi',
  'public/post.cgi'
]

  # main

  if ARGV[0] 
    ruby_path = ARGV[0]
  else
    ruby_path = `which ruby`
  end

  if ruby_path == ""
    print "cannot find Ruby command.\n"
    exit
  end
  ruby_path.gsub! /[\r\n]+/, ''

  for f in FILES
    fp = File.open(f, "r")
    fq = Tempfile.new("boardpath")

    line = fp.gets
    fq.print "#!", ruby_path, "\n"

    while line = fp.gets
      line.gsub! /[\r\n]+/, ''
      fq.print line, "\n"
    end
    fq.chmod(0705)
    fq.close
    File.rename(fq.path, fp.path)
  end
