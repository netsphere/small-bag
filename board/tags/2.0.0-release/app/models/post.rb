# -*- coding:utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 一つ一つの投稿
class Post < ActiveRecord::Base
  belongs_to :board

  # 投稿者
  belongs_to :user

  has_many :attach_files
  has_many :replies, :foreign_key => "reply_to", :class_name => "Post"

  validates_presence_of :body

end
