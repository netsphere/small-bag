# -*- coding: utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# スレッド表示のための木
class ThreadTree
  attr_reader :value
  attr_reader :children

  def initialize x
    @value = x
    @children = []
  end

  def find post_id
    raise TypeError if !post_id.is_a?(Integer)

    return self if value && value.id == post_id
    @children.each do |node|
      r = node.find post_id
      return r if r
    end
    return nil
  end

  def << post
    @children << ThreadTree.new(post)
  end
end
