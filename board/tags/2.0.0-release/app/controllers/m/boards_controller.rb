# -*- coding: utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


module M
  # mobile用の掲示板
  class BoardsController < ApplicationController

    layout "mobile"

    before_filter :login_required
    before_filter :board_must_be_owned_by_ug

    verify :method => :post, :only => [ :create_post ],
           :redirect_to => {:controller => "welcome", :action => "index"},
           :add_flash => {"alert" => "HTTP method incorrect."}


    # 投稿の一覧表示
    def show
      @board = Board.find params[:id]

      @posts = Post.paginate :page => params[:page], :per_page => 10,
                           :conditions => ["board_id = ?", @board.id],
                           :order => "created_at DESC"
    end


    # 新しいスレッド
    def new_post
      @board = Board.find params[:board_id]
      @post = Post.new :reply_to => params[:reply]
    end


    # 投稿の作成
    def create_post
      @board = Board.find params[:board_id]
      @post = Post.new params[:post]
      @post.board = @board
      @post.user = current_user

      begin
        @post.save!
      rescue ActiveRecord::RecordInvalid
        flash[:alert] = "本文がありません。"
        render :action => "new_post"
        return
      end

      flash[:notice] = "投稿しました。"
      redirect_to :action => "show", :id => @board
    end


    def show_post
      @board = Board.find params[:board_id]
      @post = Post.find params[:id]
    end
  end

end # module M
