# -*- coding: utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 投稿。投稿の編集だけ。
class PostsController < ApplicationController

  before_filter :login_required
  before_filter :board_must_be_owned_by_ug

  verify :method => :post, :only => [ :create, :destroy ],
         :redirect_to => {:controller => "welcome", :action => "index"},
         :add_flash => {"alert" => "HTTP method incorrect."}

=begin
  # GET /posts
  # GET /posts.xml
  def index
    @posts = Post.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @posts }
    end
  end
=end


=begin
  # GET /posts/1
  # GET /posts/1.xml
  def show
    @post = Post.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @post }
    end
  end
=end


=begin
  # GET /posts/new
  # GET /posts/new.xml
  def new
    @post = Post.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @post }
    end
  end
=end


=begin
  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
  end
=end


  # 投稿
  def create
    # raise # DEBUG
    @board = Board.find params[:board_id]
    @post = Post.new params[:post]
    @post.board = @board
    @post.user = current_user

=begin
    # 念のため. TODO: ユーザに入力させないようにする
    @post.replies = @post.replies.split(/[ \t\r\n]+/).delete_if do |re|
      !( (p = Post.find_by_id(re)) && p.board_id == @board.id )
    end.join(" ")
=end

    begin
      Post.transaction do 
        @post.save!
        for i in 1..2 
          if !params["file#{i}"].blank?
            af = AttachFile.new :post => @post,
                  :content_type => params["file#{i}"].content_type,
                  :original_filename => params["file#{i}"].original_filename,
                  :body => params["file#{i}"].tempfile.read,
                  :created_by => current_user
            af.save!
          end
        end
      end # transaction
    rescue ActiveRecord::RecordInvalid
      flash[:alert] = "本文がありません。"
      redirect_to :controller => "boards", :action => "show", :id => @board

      # render :controller => "boards", :action => "show", :id => @board
      return
    end

    flash[:notice] = "投稿しました。"
    redirect_to :controller => "boards", :action => "show", :id => @board
  end


=begin
  # PUT /posts/1
  # PUT /posts/1.xml
  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to(@post, :notice => 'Post was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @post.errors, :status => :unprocessable_entity }
      end
    end
  end
=end


  # DELETE /posts/1
  # DELETE /posts/1.xml
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to(posts_url) }
      format.xml  { head :ok }
    end
  end
end
