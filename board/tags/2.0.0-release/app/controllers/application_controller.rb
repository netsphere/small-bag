# -*- coding:utf-8 -*-

# Q's Web Board
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "wafu2/authenticated_system"


class ApplicationController < ActionController::Base
  protect_from_forgery
  include AuthenticatedSystem

  # remember me
  before_filter :login_from_cookie

  # @override
  def redirect_to_login
    flash[:alert] = "ログインしてください。" # + request.request_uri

    session[:return_to] = 
            ::WebBoard::Application.config.app_root_url + request.request_uri
    redirect_to ::WebBoard::Application.config.accounts_app_url + "/account/login"
    # return false
  end


  # for before_filter
  def board_must_be_owned_by_ug
    board = Board.find( params[:board_id] || params[:id] )
    g = UserGroupsUser.find :first,
                   :conditions => [ "user_id = ? AND user_group_id = ?",
                                    current_user.id, board.user_group.id ]
    if g
      true
    else
      render :action => "forbidden"
      return false
    end
  end

end
