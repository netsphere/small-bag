
-- ユーザグループ
CREATE TABLE user_groups (
  id      serial PRIMARY KEY,
  urlkey  VARCHAR(30) NOT NULL UNIQUE,

  -- 表示名
  name    VARCHAR(40) NOT NULL,

  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);
