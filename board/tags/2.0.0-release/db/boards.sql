
-- 掲示板
CREATE TABLE boards (
  id            serial PRIMARY KEY,

  -- グループに対しては、アプリケーション有効／無効を選択
  user_group_id int NOT NULL REFERENCES user_groups (id),

  -- タイトル
  name          VARCHAR(100) NOT NULL,

  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);
