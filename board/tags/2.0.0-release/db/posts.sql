
-- 一つ一つの投稿
CREATE TABLE posts (
  id         serial PRIMARY KEY,
  board_id   int NOT NULL REFERENCES boards (id),

  -- 投稿ユーザ. 別の人は編集不可
  user_id    int NOT NULL REFERENCES users (id),

  -- 本文
  body       text NOT NULL,

  -- 返信。
  reply_to   int REFERENCES posts (id),

  created_at TIMESTAMP NOT NULL

  -- 更新できないようにする
  -- updated_at TIMESTAMP
);
