# -*- coding: utf-8 -*-

# テスト用にユーザなどを作成

UserGroup.transaction do 
  ug = UserGroup.new :name => "test user group<<<"
  ug.urlkey = "ug1"
  ug.save!

  u = User.new :name => "test user<<<"
  u.login = "usr1"
  u.save!

  ugu = UserGroupsUser.new :user_group => ug, :user => u, 
                                              :email_addr => "hoge@localhost"
  ugu.save!

  b = Board.new :name => "test board<<<", :user_group => ug
  b.save!
end # transaction
