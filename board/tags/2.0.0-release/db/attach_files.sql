
-- 添付ファイル
CREATE TABLE attach_files (
  id      serial PRIMARY KEY,
  post_id int NOT NULL REFERENCES posts (id),

  original_filename VARCHAR(250) NOT NULL,
  content_type      VARCHAR(60) NOT NULL,
  body              BYTEA NOT NULL,

  created_at        TIMESTAMP NOT NULL,
  create_user_id    int NOT NULL REFERENCES users (id)
);
