
-- ユーザ
CREATE TABLE users (
  id    serial PRIMARY KEY,
  login VARCHAR(40) NOT NULL UNIQUE,

  -- 表示名
  name  VARCHAR(40) NOT NULL,

  -- メール配信用 => user_groups_users へ移動した
  -- email VARCHAR(40) NOT NULL UNIQUE,

  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);
