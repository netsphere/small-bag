# -*- coding: utf-8 -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class Profile
  attr_reader :userid
  attr_accessor :name, :website, :mail, :mob_mail, :last_access_time
  attr_accessor :admin, :admin_hashed_pwd

  def initialize hash
    hash_ = hash.dup
    @userid = hash_.delete :userid
    @name = hash_.delete :name
    @website = hash_.delete :website
    @mail = hash_.delete :mail
    @mob_mail = hash_.delete :mob_mail
    @last_access_time = hash_.delete :last_access_time
    @admin = hash_.delete(:admin).to_i != 0 ? true : false
    @admin_hashed_pwd = hash_.delete(:admin_hashed_pwd)

    raise ArgumentError, "unknown key: #{hash_.keys}" if !hash_.empty?
  end


  class << self
    # 行をパースし、Profileオブジェクトを返す
    def parse userid, line
      raise TypeError if !userid.is_a?(String)
      raise TypeError if !line.is_a?(String)

      a = line.split("\t")
      a.map! {|e| CGI.unescape(e)}
      return Profile.new :userid => userid, 
                         :name => a[0],
                         :website => a[1],
                         :mail => a[2],
                         :mob_mail => a[3], 
                         :last_access_time => a[4],
                         :admin => a[5],
                         :admin_hashed_pwd => a[6]
    end
  end # << self


  def to_s
    [ CGI.escape(name), CGI.escape(website || ""),
      CGI.escape(mail || ""), CGI.escape(mob_mail || ""),
      last_access_time,
      (admin ? "1" : "0"), admin_hashed_pwd ].join("\t")
  end
end


# useridのユーザが見つからない時はnilを返す
def get_profile userid
  raise TypeError, "expected a String, but #{userid.class}" if !userid.is_a?(String)

  StrStore.new(PRIV_DIR + "/kname").transaction_ro do |profdb|
    return nil if !profdb.key?(userid)
    return Profile.parse(userid, profdb[userid])
  end
end
