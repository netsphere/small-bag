# -*- coding:utf-8 -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def strgets_ja(id)
  case id
  when 1
    return <<EOF
<p>次の投稿を削除しました。
<ul>
EOF

  when 2
    return "<p>パスワードが違います。"

  when 3
    return <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
  <LINK rel="start" href="http://www.nslabs.jp/">
  <LINK rel="stylesheet" href="./default.css" type="text/css">
  <META name="ROBOTS" content="NOINDEX, NOFOLLOW">
  <title>%s</title>
EOF

  when 4
    return <<EOF
<div class="msg-header">
  &nbsp; <a name="%s">[%s]</a>
</div>
(削除)
EOF

  when 5
    return <<EOF
[返信]
EOF

  when 6
    return <<EOF
<span class="reply">への返信</span><br>
EOF

  when 7
    return <<EOF
<span class="attach">添付ファイル: </span>
<a href="./index.rb?action=file&amp;article=%s" target="attach">%s</a> (%sバイト)<br>
EOF

  when 8
    return <<EOF
  <tr><th align="right">管理者パスワード：
    <td>
      <input type="password" name="pwd"> &nbsp;
      <input type="submit" value="削除">
EOF

  when 14
    return "[前の %s 件]"

  when 15
    return "[次の %s 件]"

  when 18
    return <<EOF
Subject: =?ISO-2022-JP?B?%s?=
Content-Type: text/plain; charset=ISO-2022-JP
EOF

  when 19
    return "<p>error:「名前」欄が未入力。"

  when 20
    raise
    # return "text/plain; charset=euc-jp"

  when 21
    return <<EOF
<h1>検索結果</h1>
<hr>
EOF

  when 22
    return "[戻る]"

  when 23
    raise 
  end
end


def strgets_en(id)
  case id
  when 1
    s = <<EOF
<p>The following post(s) was deleted.
<ul>
EOF
    return s

  when 2
    return "<p>Incorrect password."

  when 3
    return <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
  <LINK rel="start" href="http://www.nslabs.jp/">
  <LINK rel="stylesheet" href="./default.css" type="text/css">
  <META name="ROBOTS" content="NOINDEX, NOFOLLOW">
  <title>%s</title>
EOF

  when 4
    return <<EOF
<div class="msg-header">
  &nbsp; <a name="%s">[%s]</a>
</div>
(Deleted)
EOF

  when 5
    return <<EOF
[Reply]
EOF

  when 6
    return <<EOF
<span class="reply"> Reply</span><br>
EOF

  when 7
    return <<EOF
<span class="attach">Attach: </span>
<a href="./index.rb?action=file&amp;article=%s" target="attach">%s</a> (%s bytes)<br>
EOF

  when 8
    return <<EOF
  <tr><th align="right">Admin password:
    <td>
      <input type="password" name="pwd"> &nbsp;
      <input type="submit" value="Delete">
EOF

=begin
  when 9
    return <<EOF
<form method="post" enctype="multipart/form-data" action="./board.cgi">
  <input type="hidden" name="action" value="post">
<table class="form">
  <tr><th nowrap align="right">Name:<td>
EOF
=end

  when 10
    return <<EOF
<tr><th align="right">Contrib#:
  <td><input type="text" name="re" size="20" value="%s">
<tr><th align="right" valign="top" nowrap>Text:
EOF

  when 11
    return <<EOF
<tr><th align="right">Attached file:
  <td><input type="file" name="file" size="30">
EOF

  when 12
    return <<EOF
<tr>
  <th align="right" nowrap>Deliver<br>email:
    <td><input type="checkbox" name="deliver">
EOF

  when 13
    return <<EOF
<tr><td colspan="2" align="center">
  <hr>
  <input type="submit" name="submit" value="Post"> &nbsp;
  <input type="reset" value="Clear"> ||
EOF

  when 14
    return "[Previous %s posts]"

  when 15
    return "[Next %s posts]"

  when 16
    return <<EOF
  <a href="./board.cgi">[The latest %s posts]</a>
</table>
  <input type="hidden" name="number" value="%s">
</form>
<ul>
  <li>You can safely ignore the Contrib# field, but you can write plural, space delimited, post numbers in the field.
  <li>As for quotation, use the '&gt;'. (it's added automatically when you click the [Reply] link.)
  <li>Be patient when pushing the [Post] button. It slow, specially when you've attached a file.
</ul>
EOF

  when 17
    return <<EOF
<form method="post" action="./search.rb">
  <table class="form">
    <tr><th>Search:
      <td><input type=text name="text"> <input type="submit" value="Search">
  </table>
</form>
EOF

  when 18
    return <<EOF
Subject: =?ISO-8959-1?B?%s?=
Content-Type: text/plain; charset=iso-8859-1
EOF

  when 19
    return "<p>error: Unknown user."

  when 20
    raise
    # return "text/plain; charset=iso-8859-1"
    
  when 21
    return <<EOF
<h1>Search result</h1>
<hr>
EOF

  when 22
    return "[Back]"

  when 23
    raise
  end
end


def strgets(id)
  lc = LC_MESSAGES || $LANG
  if lc == "en"
    strgets_en(id)
  else
    strgets_ja(id)
  end
end
