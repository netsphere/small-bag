#!/usr/local/bin/ruby
# -*- coding:utf-8; mode:ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "path-conf.rb"
require APP_ROOT + "/app/board-sub"
require "common"
require "injector"
require "message"

$SAFE = 1
ENV['PATH'] = '/bin'


def deliver(mes, toaddr)
  # メール配信する
  head = {
    "Subject" => DELIVER_SUBJECT,
    "Content-Type" => "text/plain; charset=UTF-8",
    "From" => DELIVER_FROM,
    "To" => toaddr.join(','),
    "MIME-Version" => "1.0",
    "Errors-To" => ADMIN_MAIL,
    "Reply-To" => ADMIN_MAIL
  }
  send_mail head, mes.body
end


def post cgi, user
  raise ArgumentError if !user || user == ""

  re_num = cgi.get("re", "").split(/[ \t]+/)
  body = cgi.get("body", "")
  attach = cgi['file'].first

  if body == ""
    set_flash cgi, "alert", "本文がありません。"
    redirect_to cgi, "./"
    exit
  end

  # 改行は削除しておく
  user.gsub! /[\r\n]/, ''

  # 同一内容？
  File.open(PRIV_DIR + "/check", File::RDWR | File::CREAT) {|check|
    check.flock(File::LOCK_EX)
    ch_data = check.readlines
    if body == ch_data.join.strip
      set_flash cgi, "alert", "二重投稿です。"
      redirect_to cgi, "./"
      exit
    end
    check.reopen(PRIV_DIR + "/check", "w+")
    check.puts(body)
  }

  now = Time.now
  counter = incr_counter(PRIV_DIR + "/counter")

  # 添付ファイルを生成する
  have_attach = false
  if attach
    if defined?(attach.length)
      len = attach.length    # StringIO
    else
      st = attach.stat       # Tempfile
      len = st.size
    end
    if len > 0 && len < 1000000
      have_attach = true
      extname = "." + get_ext_name(attach.original_filename.gsub(/\\/, '/'), "attach")

      a_fname = BOARD_ATTACH_DIR + "/" + mesno(counter) + extname
      a_fname.untaint
      File.open(a_fname, "w") {|a_file|
        attach.rewind # .sizeでストリームの末尾に移動している
        a_file.write(attach.read)
      }
    end
  end

  # 発言を生成する
  new_mes = Message.new
  new_mes.head['from'] = user
  new_mes.head['date'] = CGI.rfc1123_date(now)

  # 返信
  if re_num && re_num.size > 0
    new_mes.head['in-reply-to'] = ""
    re_num.each {|re|
      if re.to_i > 0
        # 返信先がないときは単に無視          
        next if !MDB.exist?(re)

        # 返信先に返信元を付ける
        fol_fname = PRIV_DIR + "/data/" + mesno(re) + ".follow"
        File.open(fol_fname, "a") {|fol_file|
          fol_file.flock(File::LOCK_EX)
          fol_file.print "#{counter}\n"
        }

        new_mes.head['in-reply-to'] << " #{re}"
      end
    }
  end

  if have_attach != false
    new_mes.head['attach'] = File.basename(attach.original_filename.gsub(/\\/, '/'))
    new_mes.head['attach-file'] = "#{mesno(counter)}#{extname}"
  end

  if cgi.get("bodytype") == "html"
    new_mes.head["content-type"] = "text/html; charset=UTF-8"
  else
    new_mes.head["content-type"] = "text/plain; charset=UTF-8"
  end
  new_mes.body = body.split(/\r?\n/)

  if !cgi.get('deliver').empty?
    toaddr = Array.new
    StrStore.new(PRIV_DIR + "/kname").transaction_ro do |profdb|
      profdb.keys.each {|k|
        prof = Profile.parse k, profdb[k]
        toaddr << prof.mob_mail if prof.mob_mail && prof.mob_mail != ""
      }
    end
    new_mes.head['cc'] = toaddr.join(',')
    deliver(new_mes, toaddr)
  end
  MDB.write(new_mes, counter)

  # 表示を更新する
  redirect_to cgi, "./"
end


# main
begin
  cgi = CGI.new
  userid = ENV['REMOTE_USER']
  access_log userid 
  prof = check_logined cgi, userid

  if cgi.request_method != "POST"
    redirect_to cgi, "./"
    exit
  end

  case cgi.get("action")
  when "post"
    post cgi, prof.name
  else
    raise
  end
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end
