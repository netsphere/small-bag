#!/usr/local/bin/ruby
# -*- coding:utf-8 -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ユーザ一覧

require "path-conf.rb"
require APP_ROOT + "/app/board-sub"
require "html/template"

$SAFE = 1


def users_list cgi, current_user
  profs = {}
  StrStore.new(PRIV_DIR + "/kname").transaction_ro {|profdb|
    profdb.keys.each {|k|
      profs[k] = Profile.parse k, profdb[k]
    }
  }

  tmpl = HTML::Template.new APP_ROOT + "/templates/users.html"
  profs.each {|userid, prof|
    h = {}
    h["name"] = CGI.escapeHTML(prof.name)
    if prof.website && prof.website != ""
      h["site"] = "<a href=\"" + prof.website + "\">" +
                  CGI.escapeHTML(prof.website) + "</a>"
    else
      h["site"] = "-"
    end
    if prof.mail && prof.mail != ""
      h["mail"] = "<a href=\"mailto:" + prof.mail + "\">" +
                             CGI.escapeHTML(prof.mail) + "</a>"
    else
      h["mail"] = "-"
    end
    h["mail2"] = ( if prof.mob_mail && prof.mob_mail != ""
                     CGI.escapeHTML(prof.mob_mail)
                   else
                     "-"
                   end )
    h["admin"] = prof.admin ? "管理者" : ""

    if (tim = Time.parse(prof.last_access_time)) != nil
      tim.localtime
      h["last-time"] = sprintf("%04d.%02d.%02d %02d:%02d", tim.year, tim.mon, tim.day,
                                                           tim.hour, tim.min)
    else
      h["last-time"] = "-"
    end

    if current_user == userid
      h["setting_link"] = "<a href=\"setting.rb\">[設定...]</a>"
    else
      h["setting_link"] = ""
    end

    tmpl.expand({"list" => h})
  }
  cgi_header_out cgi
  print tmpl.output
end


#main
begin
  cgi = CGI.new
  userid = ENV["REMOTE_USER"]
  access_log userid
  prof = check_logined cgi, userid

  users_list cgi, userid
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end
