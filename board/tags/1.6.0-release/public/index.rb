#!/usr/local/bin/ruby
# -*- coding:utf-8; mode:ruby -*-

# Q's WebBoard
# Copyright (c) 1999-2002,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end

require File.rel2abs("./path-conf.rb", __FILE__)
require APP_ROOT + "/app/board-sub"
require "common"
require "injector"

$SAFE = 1
ENV['PATH'] = '/bin'


# エントリのツリー
class Node
  def initialize(mesno)
    @me = mesno
    @parents = Array.new
    @children = Array.new
  end
  attr_accessor :me, :parents, :children
end


def board_thread_sub(already, node, first, last)
  if already[node.me]
    print <<EOF
<div class="msg-header">
&nbsp; <i><a href="##{mesno(node.me)}">[#{mesno(node.me)}] alias</a></i>
</div>
EOF
    return
  end
  if board_msg_1_put(node.me, first, last) > 0
    already[node.me] = true
    if node.children.size > 0
      print "<blockquote>\n"
      node.children.sort {|x, y| x.me <=> y.me}.each {|child|
        board_thread_sub(already, child, first, last)
      }
      print "</blockquote>\n"
    end
  end
end


# スレッド表示
def board_thread first, last
  print '<form method="post" action="./entries-delete.rb">', "\n"

  nodes = Array.new
  for i in 0 .. (last - first)
    nodes[i] = Node.new(first + i)
    begin
      mes = get_entry(first + i)
      if mes.get_re
        mes.get_re.each {|p|
          if p.to_i >= first && p.to_i < first + i
            nodes[i].parents << p.to_i
            nodes[p.to_i - first].children << nodes[i]
          end
        }
      end
    rescue SystemCallError
    end
  end
  already = {}
  (last - first).downto(0) {|i|
    if nodes[i].parents.size == 0
      board_thread_sub(already, nodes[i], first, last)
    end
  }
  board_msg_footer()
end


# リスト表示
def board_msg_put(first, last)
  print '<form method="post" action="./entries-delete.rb">', "\n"

  last.downto(first) {|i|
    board_msg_1_put(i, first, last)
  }
  board_msg_footer()
end


# 画面表示
def view_out cgi, user, page_start, reply = nil
  if !page_start
    page_start = [get_counter(PRIV_DIR + "/counter") - BOARD_PAGE_SIZE + 1, 1].max
  end

  html_header_out cgi, BBS_TITLE

  # 投稿フォームを表示
  print board_post_pane(cgi, user, page_start, reply)

  # 発言
  last = [page_start + BOARD_PAGE_SIZE - 1, get_counter(PRIV_DIR + "/counter")].min
  if USE_THREAD != false
    board_thread(page_start, last)
  else
    board_msg_put(page_start, last)
  end

  # フッター
  html_footer_out()
  exit
end


def attach_file_out cgi, article
  msg = get_entry article
  if msg.head['attach-file']
    vafname = "/" + File.basename(msg.head['attach-file'])
  else
    vafname = "/" + mesno(article) + ".attach"
  end
  vafname.untaint
  attach_length = FileTest.size?(BOARD_ATTACH_DIR + vafname)
  if attach_length != nil
    extname = get_ext_name(BOARD_ATTACH_DIR + vafname)
    if extname == "rb" || extname == "pl" || extname == "cgi"
      print "Content-Type: application/octet-stream\n"
      print "Content-Length: #{attach_length}\n"
      print "\n"

      fp = File.open(BOARD_ATTACH_DIR + vafname, "r")
      fp.binmode
      $stdout.binmode
      $stdout.write(fp.read)
    else
      # content-typeはWebサーバーに決めさせる
      print cgi.header("status" => "REDIRECT", 
                       "Location" => BOARD_ATTACH_PUBLIC_DIR + vafname)
      print "\n"
    end
  else
    print cgi.header("status" => "NOT_FOUND")
    print "\n"
  end
  exit
end


# main
begin
  cgi = CGI.new
  userid = ENV['REMOTE_USER']
  access_log userid
  prof = check_logined cgi, userid

  case cgi.get("action")
  when "file"
    # 添付ファイルを見る
    article = cgi.get("article")
    attach_file_out cgi, article
  when "next"
    page_start = [cgi.get("cur").to_i + BOARD_PAGE_SIZE,
                  get_counter(PRIV_DIR + "/counter")].min
    view_out cgi, prof.name, page_start
  when "prev"
    page_start = [cgi.get("cur").to_i - BOARD_PAGE_SIZE, 1].max
    view_out cgi, prof.name, page_start
  when "reply"
    # 返信
    article = cgi.get("article")
    view_out cgi, prof.name, nil, article
  when "follow"
    article = cgi.get("article").to_i
    page_start = [article - BOARD_PAGE_SIZE / 2, 1].max
    view_out cgi, prof.name, page_start
  else
    view_out cgi, prof.name, nil
  end
rescue ScriptError
  error_bt_out
rescue StandardError
  error_bt_out
end

