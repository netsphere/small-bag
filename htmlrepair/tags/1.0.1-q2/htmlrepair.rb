=begin

= HTML Repair Library

htmlrepair.rb

Version 1.0.1

Copyright (C) 2000 MoonWolf Development

MoonWolf <moonwolf-ruby@moonwolf.com>

  省略された終了タグを補う。

== 使い方

  obj = HTMLSplit.new(html)
  obj.repair

=end

require "htmlsplit"

class HTMLSplit
  CONTENT_FLOW = %w(div center blockquote ins del dd li form button th td iframe noscript)
  NOT_FLOW = %w(area param dt dd li optgroup option legend
                caption col colgroup thead tfoot tbody tr th td)
  CONTENT_INLINE = %w(tt i b u s strike big small
                      em strong dfn code samp kbd var cite abbr acronym
                      sub sup span bdo font a p h1 h2 h3 h4 h5 h6 pre q dt label
                      legend caption)
  BLOCK = %w(p h1 h2 h3 h4 h5 h6 ul ol dir menu pre dl div center noscript noframes
                  blockquote form isindex hr table fieldset address)
	
	PARENTTAG = {
		'a'			=>	%w(body),
		'thead'		=>	%w(table),
		'tfoot'		=>	%w(table),
		'tbody'		=>	%w(table),
		'tr'		=>	%w(table thead tfoor tbody),
		'td'		=>	%w(tr),
		'th'		=>	%w(tr),
		'li'		=>	%w(ol ul),
		'dt'		=>	%w(dl),
		'dd'		=>	%w(dl),
		'col'		=>	%w(colgroup),
		'param'		=>	%w(applet),
		'area'		=>	%w(map),
		'input'		=>	%w(form),
		'textarea'	=>	%w(form),
		'button'	=>	%w(form),
		'select'	=>	%w(form),
		'keygen'	=>	%w(form),
		'label'		=>	%w(form),
    'option'   => %w(select optgroup),
    'optgroup' => %w(select),
	}
	
	def repair
		tag = []
		doc = []
		@document.each {|e|
			case e
			when StartTag, EmptyElementTag
				if PARENTTAG[e.name] && (a = tag.rindex(e.name))
					#ネストか終了タグの省略かチェック
					flag = true
					tag[a..-1].each {|t|
						if PARENTTAG[e.name].include?(t)
							#正常なネスト
							flag = false
							break
						end
					}
					if flag
						#省略された終了タグを出力
						while t=tag.pop
              doc.push EndTag.new(t)
							if t==e.name
								break
							end
						end
					end
				end

        if NOT_FLOW.include?(e.name)
          while CONTENT_FLOW.include?(tag.last) || CONTENT_INLINE.include?(tag.last)
            doc.push EndTag.new(tag.pop)
          end
        elsif BLOCK.include?(e.name)
          while CONTENT_INLINE.include?(tag.last)
            doc.push EndTag.new(tag.pop)
          end
        end

				#
        tag.push e.name if e.is_a?(StartTag)
				doc.push e
			when EndTag
				if tag.include?(e.name)
					while t = tag.pop
						if t==e.name
							break
						else
              doc.push EndTag.new(t)
					end
				end
				doc.push e
				end
			else
				doc.push e
			end
		}
		while t = tag.pop
			doc.push EndTag.new(t)
		end
		@document = doc
	end
end
