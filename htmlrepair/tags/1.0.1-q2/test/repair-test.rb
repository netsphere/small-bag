
# htmlrepairのテスト
#     htmlrepair -- http://www.moonwolf.com/ruby/

require "test/unit/testcase"
require "test/unit/ui/console/testrunner"

require "htmlrepair.rb"

class HtmlRepairTest < Test::Unit::TestCase
  # 終了タグの省略
  def test11
    html = "<html>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<html></html>", r.document.to_s)
  end

  def test12
    html = "<p>a<p>b"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<p>a</p><p>b</p>", r.document.to_s)
  end

  # pタグ（内容が%inline）はulの前で終了
  # v1.0.1は失敗。   => <p>hoge<ul>foo<li>funi</li></ul></p>
  def test21
    html = "<p>hoge<ul>foo<li>funi</ul>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<p>hoge</p><ul>foo<li>funi</li></ul>", r.document.to_s)
  end

  # v1.0.1は失敗   =>   <p>hoge<table></table></p>
  def test23
    html = "<p>hoge<table></table>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<p>hoge</p><table></table>", r.document.to_s)
  end

  # liの子は%flowだが、%flowにliは含まれない。
  def test31
    html = "<ul><li>a<li>b<li>c</ul>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<ul><li>a</li><li>b</li><li>c</li></ul>", r.document.to_s)
  end

  # ulは%blockで、liの子になれる
  def test32
    html = "<li><ul>foo</li>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<li><ul>foo</ul></li>", r.document.to_s)
  end

  # p (%block) はdivの子になれる
  def test33
    html = "<div><p>foo</div>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<div><p>foo</p></div>", r.document.to_s)
  end

  # 入れ子になる
  # v1.0.1は失敗。
  def test33a
    html = "<fieldset><legend>hoge<fieldset>hogehoge"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<fieldset><legend>hoge</legend><fieldset>hogehoge</fieldset></fieldset>",
                 r.document.to_s)
  end

  # 来てはならないタグ。
  # v1.0.1は失敗。
  def test34
    html = "<li>foo<dd>bar"
    r = HTMLSplit.new(html)
    r.repair
    assert_equal("<li>foo</li><dd>bar</dd>", r.document.to_s)
  end

  # 知らないタグ
  def test35
    html = "<foo>hoge<bar>hogehoge"
    r = HTMLSplit.new(html)
    r.repair
    assert_equal("<foo>hoge<bar>hogehoge</bar></foo>", r.document.to_s)
  end

  # 正しく入れ子になっていない
  # v1.0.1は失敗  =>  <font>hoge<i>foo</i></font>bar</i>
  def test41
    html = "<font>hoge<i>foo</font>bar</i>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<font>hoge<i>foo</i></font>bar", r.document.to_s)
  end

  # 余分な終了タグ
  def test42
    html = "<foo>hoge<bar>hoge</baz>fuga"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<foo>hoge<bar>hogefuga</bar></foo>", r.document.to_s)
  end

  # 表
  def test51
    html = "<tr><td>foo<tr><td>bar"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<tr><td>foo</td></tr><tr><td>bar</td></tr>", r.document.to_s)
  end

  # v1.0.1は失敗  <th>foo<td>bar</td></th>
  def test52
    html = "<th>foo<td>bar"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<th>foo</th><td>bar</td>", r.document.to_s)
  end

  # empty tag
  # v1.0.1は失敗  <p>foo<hr>bar</p>
  def test61
    html = "<p>foo<hr>bar"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<p>foo</p><hr>bar", r.document.to_s)
  end

  # v1.0.1は失敗
  def test71
    html = "<select><optgroup><option>foo<option>bar<optgroup><option>baz</select>"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<select><optgroup><option>foo</option><option>bar</option></optgroup><optgroup><option>baz</option></optgroup></select>", r.document.to_s)
  end

  def test72
    html = "<a>foo<a>bar"
    r = HTMLSplit.new(html); r.repair
    assert_equal("<a>foo</a><a>bar</a>", r.document.to_s)
  end
end

Test::Unit::UI::Console::TestRunner.new(HtmlRepairTest).start()
