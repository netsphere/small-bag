# -*- coding: utf-8 -*-

# require 'cgi'   # escapeHTML()


# ノードの基底クラス
class HTML_NodeBase
  def initialize text
    @text = text
  end

  # @return [String] テキスト. その意味はサブクラスによる。
  attr_reader :text

  # @return [String] HTMLに戻した文字列.
  def to_s
    raise "must override"
  end
end


# タグ. StartTag, EmptyElementTagの基底クラス
#
# @example 属性の設定
#   img = EmptyElementTag.new "img"
#   img["src"] = "xxx.png"        #=> <img src="xxx.png">
#   o = EmptyElementTag.new "option"
#   o["selected"] = true          #=> <option selected>
#
class HTML_ElementTag < HTML_NodeBase

  class << self
    ESCAPE_TABLE = {
      '&' => '&amp;',
      '<' => '&lt;',
      '>' => '&gt;',
      '"' => '&quot;',
      "'" => '&#39;',
    }

    def escapeHTML value
      value.to_s.gsub(/[&<>"']/) { |s| ESCAPE_TABLE[s] }
    end
  end # << self

  # @param [String] name 要素(タグ)名
  # @param [Hash]   attr (属性名 => 属性値) のハッシュ.
  def initialize name, attr = nil
    @name = name.downcase
    @attr = attr
  end

  # @return [String] 要素(タグ)名
  attr_reader :name

  # @return [Hash, nil] 属性のハッシュまたはnil
  attr_reader :attr
  
  # @return [String] HTMLのタグの文字列
  def to_s
    if @attr
      "<" + @name + (@attr.keys.sort.collect do |n|
                       v = @attr[n]
                       if v == true
                         ' ' + n
                       else
                         ' ' + n + '="' + HTML_ElementTag.escapeHTML(v) + '"'
                       end
                     end).join + ">"
    else
      "<#{@name}>"
    end
  end


  # @param key   属性名
  # @return keyが指す属性値. 属性がないときはnil
  def [](key)
    @attr && @attr[key]
  end


  # 属性を登録/更新する。valueがnilの場合は, 属性keyを削除する
  # @param key    属性名
  # @param value  属性値
  def []=(key, value)
    if value.nil?
      @attr && @attr.delete(key)
    else
      @attr = {} if !@attr
      @attr[key] = value
    end
  end
end


# 空要素タグ
class EmptyElementTag < HTML_ElementTag
end


# 開始タグ
class StartTag < HTML_ElementTag
end


# 終了タグ
class EndTag < HTML_NodeBase
  # @param [String] name タグ名
  def initialize name
    @name = name.downcase
  end

  # @return [String] タグ名
  attr_reader :name

  # @return [String] HTMLに戻した文字列
  def to_s
    "</#{@name}>"
  end
end


# 文字データ
class CharacterData < HTML_NodeBase
  def to_s
    @text
  end
end


# 宣言 -- doctype宣言など
class Declaration < HTML_NodeBase
  def to_s
    "<!#{@text}>"
  end
end


# コメント
class Comment < HTML_NodeBase
  # @return [String] HTMLに戻した文字列.
  def to_s
    "<!--#{@text}-->"
  end
end


# SSI <!--#name ... -->
# なお, 埋め込まれたスクリプトは認識できません。
class SSI < HTML_NodeBase
  # @return [String] HTMLに戻した文字列.
  def to_s
    "<!--#{@text}-->"
  end
end


# eRuby/ASP/JSPスクリプト
# なお, 埋め込まれたスクリプトは認識できません。
class ERuby < HTML_NodeBase
  # @return [String] HTMLに戻した文字列.
  def to_s
    "<%#{@text}%>"
  end
end


# PHPスクリプト
# なお, 埋め込まれたスクリプトは認識できません。
class PHP < HTML_NodeBase
  # @return [String] HTMLに戻した文字列.
  def to_s
    "<?#{@text}?>"
  end
end


# HTMLをパースする。
# 読み込んだHTML文書は, タグと文字データなどの配列になる. to_sメソッドでHTMLに戻せる。
# 
# @example 読み込み
#   obj = HTMlSplit.new ARGF.read
#
# @example 出力
#   obj = HTMLSplit.new "path/to/file.html"
#   obj.document.each do |e|
#     print e.to_s
#   end
#
class HTMLSplit
  # 空要素タグ (内容が空で*なければならない*要素) の初期値
  EMPTY = %w( base link meta hr br wbr img embed param source track area col 
              input keygen command
              basefont bgsound frame isindex nextid spacer )

  class << self
    # コード（整数）→文字
    # unescapeHTML()から呼び出される
    #
    # @param [String] encoding  文字コード. 
    #                     nil, "NONE", "UTF8"の場合は, 0xffより上を受け付ける。
    # @param [Integer] code コードポイント. サロゲートブロック, U+FFFE, U+FFFFは不可
    # @param unmatch  使用不可の文字の場合に評価する
    def make_char code, unmatch, encoding
      if code == 0x9 || code == 0xa || code == 0xd || code >= 0x20 && code <= 0xff
        code.chr
      else
        if (!encoding || encoding == "NONE" || encoding == "UTF8") &&
           (code <= 0xd7ff || code >= 0xe000 && code <= 0xfffd || 
                                         code >= 0x10000 && code <= 0x10ffff)
          return [code].pack("U")
        else
          unmatch
        end
      end
    end


    # entity reference (&name;) のうち, amp, quot, apos, gt, lt のみ展開する
    # character reference (&#, &#x) を展開する。
    def unescapeHTML string, encoding
      string.gsub(/&(.*?);/n) do
        match = $1.dup
        case match
        when /\Aamp\z/ni           then '&'
        when /\Aquot\z/ni          then '"'
        when /\Aapos\z/ni          then "'"
        when /\Agt\z/ni            then '>'
        when /\Alt\z/ni            then '<'
        when /\A#(\d+)\z/n
          make_char Integer($1), "&##{$1};", encoding 
        when /\A#x([0-9a-f]+)\z/ni
          make_char $1.hex, "&#x#{$1};", encoding
        else
          "&#{match};"
        end
      end
    end
  end # << self


  # 開始タグまたは空要素タグを生成する.
  # @param [String] name タグ名
  # @param [Hash]   attr 属性のハッシュ
  def make_tag name, attr
    name.downcase!
    if @empty_tags.include?(name)
      @document << EmptyElementTag.new(name, attr)
    else
      if name[0, 1] == '/'
        @document << EndTag.new(name[1..-1])
      else
        @document << StartTag.new(name, attr)
      end
    end
  end
  private :make_tag


  # HTMLSplit オブジェクトを生成する.
  #
  # @param [String, nil] html  HTML文書またはnil. 
  #       HTML文書を与えた場合は, この場でパースする. (documentメソッドで取り出せる.)
  #       空要素タグパラメータを変更したい場合は, ここではnilを与え, 空要素パラメータを
  #       変更したのちに set_htmlメソッドを呼び出すこと。
  # @param [String] encoding   文字コード. 通常は "UTF8" を与える.
  def initialize html = nil, encoding = $KCODE
    @empty_tags = EMPTY
    set_html(html, encoding) if html
  end

  # @return [Array] パース済みのHTML文書
  attr_reader :document

  # @return [Array] 空要素タグの配列
  attr_accessor :empty_tags
  

  # HTML文書をパースする。
  def set_html(html, encoding = $KCODE)
    @document = []	#パースしたHTMLのリスト
    name = ''
    text = ''
    attr = {}
    attrname = ''
    state = :TEXT

    html.each_byte do |c|
      char = c.chr
      case state
      when :TEXT  # 地の文
        if c == 60  # "<"
          if text.length > 0
            @document << CharacterData.new(text)
          end
          name = ''
          attr={}
          state = :TAGNAME
        else
          text << char
        end

      when :TAGNAME # タグの開始
        if name.length == 0
          case char
          when '!'
            text = ''
            state = :DECLARE
          when '%'
            text = ''
            state = :ERUBY
          when '?'
            text = ''
            state = :PHP
          when /[a-zA-Z_:\/]/  # "/"のときは終了タグ
            name << char
          else
            text = '<' + char
            state = :TEXT
          end
        else
          case char
          when '>'
            if name.length == 1 && name == "/"
              text = "</>"   # SGML (SHORTTAG YES) で empty end-tag だが, 
                             # サポートする必要ないだろう
            else 
              make_tag(name, nil)
              text = ''
            end
            state = :TEXT
          when /(\s)/
            if name.length == 1 && name == "/"
              text = "</" + $1
              state = :TEXT
            else
              text=''
              state = :SPACE   # 属性がある
            end
          when /[a-zA-Z0-9\._:-]/
            name << char
          else
            text = ''
            state = :SPACE
            redo
          end
        end

      when :SPACE	#属性間の空白
        case char
        when '>'
          make_tag(name, attr)
          text = ''
          state = :TEXT
        when '<'  # 閉じない開始タグ. 終了タグでも可
          make_tag(name, attr)
          name = ''
          attr = {}
          state = :TAGNAME
        when /\s/
          # skip
        else
          attrname = char
          state = :ATTRNAME
        end

      when :ATTRNAME	#属性名
        case char
        when /\s/
          state = :BEFOREEQUAL
        when '='
          state = :AFTEREQUAL
        when '>'
          if attrname != "/"  # ex) <br />
            attr[attrname.downcase] = true
          end
          make_tag(name, attr)
          text = ''
          state = :TEXT
        else
          attrname << char
        end

      when :BEFOREEQUAL	# attrname^
        case char
        when '='
          state = :AFTEREQUAL
        when '>'
          attr[attrname.downcase]=true
          make_tag(name, attr)
          text = ''
          state = :TEXT
        when /\s/
        else
          attr[attrname.downcase]=true
          attrname = char
          state = :ATTRNAME
        end

      when :AFTEREQUAL	# attrname=^
        case char
        when "'"
          text=''
          state = :SQVALUE
        when '"'
          text=''
          state = :DQVALUE
        when '>'
          attr[attrname.downcase]=true
          make_tag(name, attr)
          text = ''
          state = :TEXT
        when /\s/
        else
          text=char
          state = :VALUE
        end

      when :VALUE	# 属性値
        case char
        when /\s/
          attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
          state = :SPACE
        when '>'
          attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
          make_tag(name, attr)
          text = ''
          state = :TEXT
        else
          text << char
        end

      when :SQVALUE	#'値'
        if c==39
          attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
          state = :SPACE
        else
          text << char
        end

      when :DQVALUE	#"値"
        if c==34
          attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
          state = :SPACE
        else
          text << char
        end

      when :COMMENT
        case char
        when '>'
          if text[-2,2]=='--'	#コメント終了	
            text = text[0..-3]
            if text =~ /^#[a-z]+/	#SSI
              @document << SSI.new(text)
            else
              @document << Comment.new(text)
            end
            text = ''
            state = :TEXT
          else
            text << char
          end
        else
          text << char
        end

      when :ERUBY
        case char
        when '>'
          if text[-1,1]=='%'	#eRuby終了	
            text = text[0..-2]
            @document << ERuby.new(text)
            text = ''
            state = :TEXT
          else
            text << char
          end
        else
          text << char
        end

      when :PHP
        case char
        when '>'
          if text[-1,1]=='?'	# ?>
            text = text[0..-2]
            @document << PHP.new(text)
            text = ''
            state = :TEXT
          else
            text << char
          end
        else
          text << char
        end

      when :DECLARE
        case char
        when '>'
          @document << Declaration.new(text)
          text = ''
          state = :TEXT
        else
          text << char
          if text=='--'
            text = ''
            state = :COMMENT
          end
        end
      end
    end # each_byte
    
    #EOFの処理
    case state
    when :TEXT
      @document << CharacterData.new(text) if text.length>0
    when :TAGNAME
      @document << CharacterData.new('<'+text)
    when :SPACE	#属性間の空白
      make_tag(name, attr)
    when :ATTRNAME	#属性名
      attr[attrname.downcase]=true
      make_tag(name, attr)
    when :BEFOREEQUAL	#=
      attr[attrname.downcase]=true
      make_tag(name, attr)
    when :AFTEREQUAL	#=
      attr[attrname.downcase]=true
      make_tag(name, attr)
    when :VALUE		#値
      attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
      make_tag(name, attr)
    when :SQVALUE	#'値'
      attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
    when :DQVALUE	#"値"
      attr[attrname.downcase]=HTMLSplit.unescapeHTML(text, encoding)
    when :COMMENT
      if text=~/^#[a-zA-Z]+/	#SSI
        @document << SSI.new(text)
      else
        @document << Comment.new(text)
      end
    when :ERUBY
      @document << ERuby.new(text)
    when :PHP
      @document << PHP.new(text)
    when :DECLARE
      @document << Declaration.new(text)
    end

    return self
  end
  

  # @return [String] HTMLに戻した文字列.
  def to_s
    s = ''
    @document.each do |e|
      s << e.to_s
    end
    return s
  end


  def find_end_tag i, taglist
    e = @document[i]
    case e
    when StartTag
      taglist.push [e, i]
    when EndTag
      idx = nil
      (taglist.size - 1).downto(0) do |j|
        if taglist[j][0].name == e.name
          idx = j
          break
        end
      end

      if idx
        taglist[idx .. -1] = nil # delete
      end
    else
      # empty
    end
  end
  private :find_end_tag


  # すべてのノードをブロックに渡す
  # @yieldparam [HTML_NodeBase] node
  # @yieldparam [Array] ary   トップレベルから node の直接の親までの [開始タグ, index] の配列
  # 
  # @return [void]
  def each
    tag = []
    @document.each_with_index do |e, i|
      find_end_tag i, tag
      yield e, tag
    end 

    return nil
  end


  # ノードを検索する.
  # @deprecated 複雑すぎる
  #
  # (_start.._end) の範囲内で, クラスが node_classかつタグ名が value のノードで,
  # count番目のノードのインデックスを返す.
  # ブロックを与えた場合, ブロックが真を返したノードがヒットする.
  #
  # @param [Class] node_class 検索するノードのクラス
  # @param [Integer] _start  検索範囲の開始インデックス
  # @param [Integer] _end    検索範囲の終了インデックス. _endを含む.
  # @param value      nil以外を指定した場合, ノードのタグ名などが一致するかチェックする
  #            node_class がEmptyElementTag, StartTag, EndTag の場合はタグ名,
  #            それ以外の場合は text と比較します。
  # @param [Integer] count   count番目を検索. count = 1の場合, 最初に見つけたノード
  #
  # @yieldparam [HTML_NodeBase] node   ブロックを与えた場合, ブロックからの戻り値が
  #                      真の場合のみ, ヒットする。
  # 
  # @return [Integer, nil] 見つかった場合ノードのインデックス, 見つからなかった場合はnil
  #
  def index node_class, _start = 0, _end = -1, value = nil, count = 1
    idx = _start
    found = false
    @document[_start.._end].each do |obj|
      if obj.class == node_class
        if value
          case obj
          when StartTag, EmptyElementTag, EndTag
            if value == obj.name
              if !block_given? || yield(obj)
                return idx if (count -= 1) <= 0
              end
            end
          else
            if value == obj.text
              if !block_given? || yield(obj)
                return idx if (count -= 1) <= 0
              end
            end
          end
        else
          if !block_given? || yield(obj)
            return idx if (count -= 1) <= 0
          end
        end
      end
      idx += 1
    end 
    
    return nil
  end


  # start_indexが指す開始タグに対応する終了タグのインデックス.
  # 対応する終了タグがなかったときは nil を返す.
  #
  def end_index start_index
    if @document[start_index].class != StartTag
      raise ArgumentError
    end

    tag = []
    (start_index .. (@document.size - 1)).each do |idx|
      find_end_tag idx, tag

      if @document[idx].class == EndTag && tag.size == 0
        return idx
      end
    end
    return nil
  end
end

