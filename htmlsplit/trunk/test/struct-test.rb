# -*- coding: utf-8 -*-

require "test/unit/testcase"
require "test/unit/ui/console/testrunner"

require "htmlsplit"

class StructureTest < Test::Unit::TestCase
  # コメント
  def test11
    html = "<!--  <foo> & <bar> -->"
    o = HTMLSplit.new(html)
    assert_equal("  <foo> & <bar> ", o.document[0].text)
  end

  # 不正なコメント。Webブラウザでは、"-->"までコメントになる。
  def test12
    html = "<!-- foo -- bar -->"
    o = HTMLSplit.new(html)
    assert_equal(" foo -- bar ", o.document[0].text)
  end

  # 処理命令 (PI)
  def test21
    html = "<?hoge <foo> & <bar> ?>"
    o = HTMLSplit.new(html)
    assert_instance_of(PHP, o.document[0])
    assert_equal("hoge <foo> & <bar> ", o.document[0].text)   # PITargetは認識しない
  end

=begin
  # CDATAセクション
  def test31
  end
=end

  # 文書型宣言
  def test41
    html = '<!DOCTYPE greeting SYSTEM "hello.dtd">'
    o = HTMLSplit.new(html)
    assert_instance_of(Declaration, o.document[0])
    assert_equal('DOCTYPE greeting SYSTEM "hello.dtd"', o.document[0].text)
  end

=begin
  # 内部サブセット
  def test42
    html = '<!DOCTYPE greeting [ <!ELEMENT greeting (#PCDATA)> ]>'
    o = HTMLSplit.new(html)
    assert_instance_of(Declaration, o.document[0])
    assert_equal('DOCTYPE greeting SYSTEM "hello.dtd"', o.document[0].text)
  end
=end

  # 開始タグ
  def test51
    html = "<foo bar='hoge'>"    # '...'で囲んでもよい
    o = HTMLSplit.new(html)
    tag = o.document[0]
    assert_instance_of(StartTag, tag)
    assert_equal({"bar" => "hoge"}, tag.attr)
  end

  # 空要素タグ (XMLスタイル)
  # 制約：empty_tagsにないときはStartTagを返す。
  #       HTML（アプリケーション）とXMLは相容れない。
  def test52
    html = "<foo/>"
    o = HTMLSplit.new(html)
    tag = o.document[0]
    assert_instance_of(StartTag, tag)
    assert_equal({}, tag.attr)
  end

  # 不正なタグ
  def test53
    html = "<0> < <>"
    o = HTMLSplit.new(html)
    assert_instance_of(CharacterData, o.document[0])
    assert_equal("<0> ", o.document[0].text)
    assert_instance_of(CharacterData, o.document[1])
    assert_equal("< ", o.document[1].text)
    assert_instance_of(CharacterData, o.document[2])
    assert_equal("<>", o.document[2].text)
  end

  # 終了タグ
  def test54
    html = "</foo>"
    o = HTMLSplit.new(html)
    tag = o.document[0]
    assert_instance_of(EndTag, tag)
    assert_equal("foo", tag.name)
  end

  # SGMLでは、属性値が択一式のときは、属性名を省略できる。
  # 制約：属性名が省略された場合は、属性値=>trueを返す。
  def test55
    html = "<foo bar>"
    o = HTMLSplit.new(html)
    tag = o.document[0]
    assert_instance_of(StartTag, tag)
    assert_equal({"bar" => true}, tag.attr)
    assert_equal("<foo bar>", tag.to_s)
  end

  # 閉じない開始タグ     SGMLで規定。Webブラウザでも解釈している
  def test61
    html = "<foo<bar>"
    o = HTMLSplit.new(html)
    tag = o.document[0]
    assert_instance_of(StartTag, tag)
    assert_equal("foo", tag.name)
    tag = o.document[1]
    assert_instance_of(StartTag, tag)
    assert_equal("bar", tag.name)
  end

  # 閉じない終了タグ    SGMLで規定。Webブラウザでも解釈している
  def test62
    html = "</foo</bar>"
    o = HTMLSplit.new(html)
    tag = o.document[0]
    assert_instance_of(EndTag, tag)
    assert_equal("foo", tag.name)
    tag = o.document[1]
    assert_instance_of(EndTag, tag)
    assert_equal("bar", tag.name)
  end

  # 空要素タグと判定するタグ名を追加。
  def test71
    html = "<x:foo>"
    o = HTMLSplit.new()
    o.empty_tags << "x:foo"
    o.set_html(html)
    tag = o.document[0]

    assert_instance_of(EmptyElementTag, tag)
    assert_equal("x:foo", tag.name)
  end
  
  # to_s
  def test_s
    x = StartTag.new('a', 'b' => 'c')
    assert_equal('<a b="c">', x.to_s)
  end
  
  # 属性の設定
  def test_attr
    x = StartTag.new('a')
    assert_nil(x['b'])
    x['c'] = 'd'
    assert_equal('<a c="d">', x.to_s)
    x['c'] = nil
    assert_equal('<a>', x.to_s)
  end
end

Test::Unit::UI::Console::TestRunner.new(StructureTest).start()
