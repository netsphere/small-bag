
require "test/unit/testcase"
require "test/unit/ui/console/testrunner"

require "htmlsplit"

# JIS XML 目次
# http://www.y-adagio.com/public/standards/jis_xml/toc.html

# ＠IT：やさしく読む「XML 1.0勧告」 第22回　物理構造における「文字参照」と「実体参照」
# http://www.atmarkit.co.jp/fxml/rensai/w3cread22/w3cread22_1.html

class RefTest < Test::Unit::TestCase
  # 内容
  def test11
    html = "<e>hoge&lt;hoge</e>"
    o = HTMLSplit.new(html)
    assert_equal("hoge&lt;hoge", o.document[1].to_s)   # 取り込まない
  end

  def test12
    html = "<e>hoge&#41;hoge</e>"
    o = HTMLSplit.new(html)
    assert_equal("hoge&#41;hoge", o.document[1].to_s)  # 取り込まない
  end

  # 属性値
  def test13
    html = "<e foo='hoge&lt;hoge'>"
    o = HTMLSplit.new(html)
    assert_equal("hoge<hoge", o.document[0].attr["foo"])  # 取り込む
  end

  def test14
    html = "<e foo='&#41;'>"
    o = HTMLSplit.new(html)
    assert_equal(")", o.document[0].attr["foo"])   # 取り込む
  end

  # 第2引数を追加
  def test15
    html = "<e foo='&#x4100;'>"
    o = HTMLSplit.new(html)
    assert_equal("&#x4100;", o.document[0].attr["foo"])
    o = HTMLSplit.new(html, "UTF8")                        # UTF8の場合のみ取り込む
    assert_equal([0x4100].pack("U"), o.document[0].attr["foo"])
  end

  def test21
    html = "<e foo='&#xd7ff;'>"
    o = HTMLSplit.new(html, "UTF8")
    assert_equal([0xd7ff].pack("U"), o.document[0].attr["foo"])
    html = "<e foo='&#xd800;'>"
    o = HTMLSplit.new(html, "UTF8")
    assert_equal("&#xd800;", o.document[0].attr["foo"])   # サロゲート領域は取り込まない
  end
end

Test::Unit::UI::Console::TestRunner.new(RefTest).start()
