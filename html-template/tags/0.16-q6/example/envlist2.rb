#!/usr/bin/ruby

require "html/template"
tmpl = HTML::Template.new("envlist.html")
envlist = []
ENV.keys.sort.each do |k|
  envlist.push({
		 'key' => k,
		 'val' => ENV[k]
	       })
end
tmpl.param({'envlist' => envlist}) 
print "Content-Type: text/html\n\n"
print tmpl.output
