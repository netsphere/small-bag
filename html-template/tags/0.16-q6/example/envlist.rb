#!/usr/bin/ruby

require "html/template"

tmpl = HTML::Template.new("envlist.html")

ENV.keys.sort.each do |k|
  tmpl.node("envlist").param({
			       'key' => k,
			       'val' => ENV[k]
			     })
end
print "Content-Type: text/html\n\n"
print tmpl.output
