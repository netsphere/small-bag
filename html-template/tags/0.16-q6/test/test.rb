
require "html/template"
require "test/unit"

class TemplateTestCase < Test::Unit::TestCase
  def setup
    @tmpl = HTML::Template.new
  end
  
  # テンプレートデータ：
  #   閉じていないノードはエラー
  #   正しく入れ子になっていないノードはエラー
  def test_tmpldata
    assert_raise(ArgumentError) { @tmpl.set_html "<!begin:t><!end:e>" }
    assert_nothing_raised { @tmpl.set_html "<!begin:s><!begin:t><!end:t><!end:s>" }
    assert_raise(ArgumentError) { @tmpl.set_html "<!begin:s><!begin:t><!end:s><!end:t>" }
  end

  # Template#node():
  #   TemplateNodeインスタンスを返す。
  #   ノードが見つからないときはエラー
  def test_node
    @tmpl.set_html "<a>text</a>"
    assert_raise(ArgumentError) { @tmpl.node('a') }

    @tmpl.set_html "<!begin:t><!var:v>foo<!end:t>"
    assert_raise(ArgumentError) { @tmpl.node('x') }
    n = @tmpl.node('t')
    assert_instance_of(HTML::TemplateNode, n)
    assert_equal('t', n.name)
    
    @tmpl.set_html "<!begin:s><!begin:t><!end:t><!end:s>"
    n = @tmpl.node('s').node('t')
    assert_instance_of(HTML::TemplateNode, n)
    assert_equal('t', n.name)
  end

  # 展開データのキーが見つからない（ノード、var）ときは展開データを無視。
  def test10
    @tmpl.set_html "<a></a>"
    assert_equal("<a></a>", @tmpl.expand({"hoge" => "foo"}).output)
    
    @tmpl.set_html "<a><!var:x></a>"
    assert_equal "<a></a>", @tmpl.expand({"hoge"=>"foo"}).output

    @tmpl.set_html "<!begin:t><!end:t>"
    assert_equal "", @tmpl.expand({"hoge" => {}}).output
  end

  # 変数の展開
  # 展開されなかった<!var:...>は削除される。
  def test4
    @tmpl.set_html("<!var:foo>")
    assert_equal("bar", @tmpl.expand({"foo" => "bar"}).output)

    # 数値は、自動的に文字列に変換する
    assert_equal("20", @tmpl.expand({"foo" => 20}).output)
    
    assert_equal "", @tmpl.expand({}).output
  end
  
  # ノードの展開:
  #   展開されなかったノードは削除される。
  #   展開データとして配列を与えると、繰り返す。
  def test11
    @tmpl.set_html "<!begin:t><!var:x>foo<!end:t>"
    assert_equal "", @tmpl.expand({}).output
    
    assert_equal "foo", @tmpl.expand({'t'=>{}}).output
    
    # ノードを確認しておく
    n = @tmpl.node('t')
    assert_equal '<!var:x>foo', n.html

    assert_equal "1foo", @tmpl.expand({'t'=>{'x'=>1}}).output
    assert_equal "1foo2foo", @tmpl.expand({'t'=>[{'x'=>1}, {'x'=>2}]}).output
  end


  def sub_t11
    @tmpl.set_html '<!begin:t><!var:foo><!end:t>'
  end

  # node()を通して展開してもOK
  def test5
    sub_t11
    @tmpl.node("t").expand({"foo" => "bar"})
    assert_equal("bar", @tmpl.output)
  end

  # expand()の中にノードを書くと，直接そのノードを展開する
  def test6
    tmpl = sub_t11
    o = tmpl.expand({"t" => {"foo" => "bar"}}).output
    assert_equal("bar", o)
  end

  # 展開データ中の'<'や'>'はエスケープされない。
  def test7
    tmpl = HTML::Template.new.set_html("<!var:foo>").expand({"foo" => "<tag>"})
    assert_equal("<tag>", tmpl.output)
  end

  # 値として汚染された文字列を渡してもエラーにはならない。
  # これをいちいちエラーにしていたら、かえってミスを誘発しそう。
  def test8
    tmpl = HTML::Template.new.set_html("<!var:foo>")
    assert_equal("bar", tmpl.expand({"foo" => "bar".taint}).output)
  end
end

