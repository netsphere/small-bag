#!/usr/bin/ruby

require "html/template"

tmpl = HTML::Template.new("env.html")
tmpl.param({
	     'home' => ENV['HOME'],
	     'path' => ENV['PATH']
	   })
print "Content-Type: text/html\n\n"
print tmpl.output

