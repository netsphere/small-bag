#!/usr/bin/ruby

require "html/template"

tmpl = HTML::Template.new("filename" => "nest.html")

PARENT = [
  { "p_var" => "PARENT 1",
    "child" => [
      {
	"hoge" => "CHILD 1-1",
	"c_var" => "1-1"
      },
      {
	"hoge" => "CHILD 1-2",
	"c_var" => "1-2"
      },
      ]
  },
  { "p_var" => "PARENT 2",
    "child" => [
      {
	"hoge" => "CHILD 2-1",
	"c_var" => "2-1"
      },
      {
	"hoge" => "CHILD 2-2",
	"c_var" => "2-2"
      },
      ]
  },
  ]

tmpl.param({"parent" => PARENT})
print "Content-Type: text/html\n\n"
print tmpl.to_s
