
=begin

= HTML/Template

Ruby extension for HTML/Template

== HTML/Template

This module deals with HTML template by CGI scripts and use a complete selectivity between design and logic easily. 

You can deal with a nested block, a loop and a conditional expression.

Simpel example, see below.

template file test.html

 <HTML>
 <HEAD><TITLE>Test Template</TITLE>
 <BODY>
 My Home Directory is <!var:home>
 <P>
 My Path is set to <!var:path>
 </BODY>
 </HTML>

Ruby script
 
 require "html/template"
 tmpl = HTML::Template.new("./test.html")
 tmpl.param({
            'home' => ENV['HOME'],
            'path' => ENV['PATH']
            })
 print "Content-Type: text/html\n\n"
 print tmpl.output

== INSTALL

using install.rb 

 % tar xvzf html-template-x.xx.tar.gz
 % cd html-template-x.xx
 % ruby install.rb config
 % ruby install.rb setup
 % su
 # ruby install.rb install

== Template File

=== TAGS

You can use original style tag or valid HTML comment style tag.

--- <!var:PARAMETER_NAME>
--- <!-- var:PARAMETER_NAME -->
    When you call HTML::Template#param ({PARAMETER_NAME => VAL}), tags will be simply replaced to VAL.

--- <!begin:PARAMETER_NAME>
--- <!end:PARAMETER_NAME>
--- <!-- begin:PARAMETER_NAME -->
--- <!-- end:PARAMETER_NAME -->
    It allows you to separate a section of text and give it a name
    It would be able to make them loop or conditional expression.
    And, it can be nested.

--- <!include:FILE_NAME> 
--- <!-- include:FILE_NAME -->
    A file specified by FILE_NAME is simply inserted to a template file.
    The template file will be treated as a merged template.
    If you pass the path param to HTML::Template.new , include file will be searched under the path.

== HTML::Template CLASS

=== CLASS METHODS

--- HTML::Template.new(params = {})
    Creates a HTML::Template object.
    Given a file name to its first argument, use this file as a template.

    Also you can pass params as Hash.
    The options are below

* filename

  template filename

* path

  template search path.
  you can specified as String or Array of String.

=== METHODS

--- HTML::Template#load(file)
    Load template file.
    
--- HTML::Template#set_html(html)
    Setting up a string as a template.

--- HTML::Template#param(hash = {})
--- HTML::Template#expand(hash = {})
    Specifies a string or a loop by Hash for a substitute. 
    When specifying more than a loop simultaneously, value of the hash becomes an array.

--- HTML::Template#node(name)
--- HTML::Template#loop(name)
--- HTML::Template#cond(name)
    Stands a loop and a conditional expression specified by <!begin:name><!end:name>
    Retrieves HTML::TemplateNode object.

--- HTML::Template#output
--- HTML::Template#to_s
    Returns the final result.

== HTML::TemplateNode CLASS

This is a class for a loop or a conditional expression. 
An instance consists of HTML::Template Class and HTML::TemplateNode CLASS.

=== METHODS

--- HTML::TemplateNode#node(name)
    Stands a loop or a conditional expression specified by <!begin:name><!end:name>
    Returns HTML::TemplateNode object.
    Is only used for nested.

--- HTML::TemplateNode#param(hash = {})
--- HTML::TemplateNode#expand(hash = {})
--- HTML::TemplateNode#add(hash = {})
    Specifies a string or a loop by Hash for a substitute. 
    If you don't call the method, the block won't be displayed.

== A LOOP

This sample is a displaying list of environment valiables.

  <html>
  <body>
  <h1>Env List</h1>
  <hr>
  <!begin:envlist>
  <!var:key> : <!var:val><br>
  <!end:envlist>
  <hr>
  </body>
  </html>

  # In case of using it with iterator and replace data.
  require "html/template"
  tmpl = HTML::Template.new("filename" => "envlist.html")
  ENV.keys.sort.each do |k|
    tmpl.node("envlist").param({
                               'key' => k,
			       'val' => ENV[k]
			     })
  end
  print "Content-Type: text/html\n\n"
  print tmpl.output

  # In case of giving a loop as an array
  require "html/template"
  tmpl = HTML::Template.new("envlist.html")
  envlist = []
  ENV.keys.sort.each do |k|
    envlist.push({
                   'key' => k,
                   'val' => ENV[k]
	         })
  end
  tmpl.param({'envlist' => envlist}) 
  print "Content-Type: text/html\n\n"
  print tmpl.output


== A Conditional Expression

  <html>
  <body>
  <!begin:true>
  This is True
  <!var:foo>
  <hr>
  <!end:true>
  <!begin:false>
  This is False
  <!var:foo>
  <hr>
  <!end:false>
  </body>
  </html>

  
  require "html/template"
  tmpl = HTML::Template.new("cond.html")
  tmpl.param({
             'true' => {'foo' => 'foobar'}
	     }) 
  print "Content-Type: text/html\n\n"
  print tmpl.output

The performed result is below, however, a part not specied with the method isn't displayed.

  <html>
  <body>

  This is True
  foobar
  <hr>
  </body>
  </html>

== An attached Sample.

:env.rb
        This sample is a simply replacing valiables.

:envlist.rb
        This sample is a displaying list of environment valiables.
        This sample is a case for using with iterator.

:envlist2.rb
        Performed result is the same as the envlist.rb.'s.

:nest.rb
        A sample of a nested loop.

== THANKS

English Documentation

NAKAYAMA Nao <nao_o@netlaputa.ne.jp>

thanks a lot :-)

== AUTHOR

Copyright 2001 IKEBE Tomohiro 
This library is free software; you can redistribute it and / or modify it under the same terms as
Ruby itself.

IKEBE Tomohiro <ikechin@Oxfa.com>

=end
