
=begin

= HTML::Template

HTML をテンプレートとして扱う Ruby モジュール

== HTML::Template とは？

CGI スクリプト等から、HTML のテンプレートを扱うモジュールです。
デザインとロジックの完全な分離が簡単に出来ます。

多重にネストしたブロック(ループ、条件分岐)を扱えます。

具体的には以下のようなことが出来ます。

テンプレートファイル test.html

 <HTML>
 <HEAD><TITLE>Test Template</TITLE>
 <BODY>
 My Home Directory is <!var:home>
 <P>
 My Path is set to <!var:path>
 </BODY>
 </HTML>

Ruby スクリプト
 
 require "html/template"
 tmpl = HTML::Template.new("./test.html")
 tmpl.param({
            'home' => ENV['HOME'],
            'path' => ENV['PATH']
            })
 print "Content-Type: text/html\n\n"
 print tmpl.output

== インストール

install.rb を使用しています

 % tar xvzf html-template-x.xx.tar.gz
 % cd html-template-x.xx
 % ruby install.rb config
 % ruby install.rb setup
 % su
 # ruby install.rb install

== テンプレートファイル

=== テンプレートで使用できるタグ

全てのタグは独自形式、もしくは標準的な HTML コメントとして記述することが可能です。

--- <!var:PARAMETER_NAME>
--- <!-- var:PARAMETER_NAME -->
    HTML::Template#param({PARAMETER_NAME => VAL}) を呼ぶと、単純に VAL に変換されます。

--- <!begin:PARAMETER_NAME>
--- <!end:PARAMETER_NAME>
--- <!-- begin:PARAMETER_NAME -->
--- <!-- end:PARAMETER_NAME -->
    囲まれた範囲をブロックとして、ループ、条件分岐を行います。
    入れ子にすることも可能です。
    PARAMETER_NAME は同階層においては、ユニークである必要があります。

--- <!include:FILE_NAME>
--- <!-- include:FILE_NAME -->
    FILE_NAME で指定されたファイルの中身と置換されます。
    テンプレートファイルを指定すると、大きな一つのテンプレートとして取り扱えます。
    コンストラクタに path パラメータが渡されている場合はそこから探索されます

== HTML::Template クラス

=== クラスメソッド

--- HTML::Template.new(params = {})
    新しい HTML::Template オブジェクトを生成する。
    引数にファイル名をあたえると、そのファイルをテンプレートとして使用します。

    パラメータをハッシュとして与える事も可能です。
    以下のオプションが使用できます。

* filename

  テンプレートのファイル名を指定します

* path

  テンプレートが探されるパスを指定します。
  文字列、もしくは文字列の配列で複数指定することが可能です。


=== メソッド

--- HTML::Template#load(file)
    テンプレートファイルをロード

--- HTML::Template#set_html(html)
    文字列をテンプレートとしてセット

--- HTML::Template#param(hash = {})
--- HTML::Template#expand(hash = {})
    置換する文字列、ループをハッシュで指定する。
    ループを同時に指定する場合は、ハッシュの値は配列となる。

--- HTML::Template#node(name)
--- HTML::Template#loop(name)
--- HTML::Template#cond(name)
    <!begin:name><!end:name> で指定されたループ、条件分岐を示す HTML::TemplateNode オブジェクトを返す。

--- HTML::Template#output
--- HTML::Template#to_s
    テンプレートの最終結果を返します。

== HTML::TemplateNode クラス

ループ、条件分岐を表現するためのクラスです。
インスタンスは HTML::Template クラス、HTML::TemplateNode クラスにより生成されます。

=== メソッド

--- HTML::TemplateNode#node(name)
    <!begin:name><!end:name> で指定されたループ、条件分岐を示す HTMl::TemplateNode オブジェクトを返す。
    入れ子構造の場合に使用。

--- HTML::TemplateNode#param(hash = {})
--- HTML::TemplateNode#expand(hash = {})
--- HTML::TemplateNode#add(hash = {})

    置換する文字列、ループをハッシュで指定する。
    このメソッドを一度も呼ばなかった場合、そのブロックは表示されません。

== ループの使い方。

環境変数をリスト表示するサンプルです。

  <html>
  <body>
  <h1>Env List</h1>
  <hr>
  <!begin:envlist>
  <!var:key> : <!var:val><br>
  <!end:envlist>
  <hr>
  </body>
  </html>

  # イテレータと共に用いてデータを置換する場合
  require "html/template"
  tmpl = HTML::Template.new("envlist.html")
  ENV.keys.sort.each do |k|
    tmpl.node("envlist").param({
                               'key' => k,
			       'val' => ENV[k]
			     })
  end
  print "Content-Type: text/html\n\n"
  print tmpl.output

  # ループの値を配列として与える場合。
  require "html/template"
  tmpl = HTML::Template.new("envlist.html")
  envlist = []
  ENV.keys.sort.each do |k|
    envlist.push({
		 'key' => k,
		 'val' => ENV[k]
	         })
  end
  tmpl.param({'envlist' => envlist}) 
  print "Content-Type: text/html\n\n"
  print tmpl.output


== 条件分岐について

  <html>
  <body>
  <!begin:true>
  こちらは True です。
  <!var:hoge>
  <hr>
  <!end:true>
  <!begin:false>
  こちらは False です。
  <!var:hoge>
  <hr>
  <!end:false>
  </body>
  </html>

  
  require "html/template"
  tmpl = HTML::Template.new("cond.html")
  tmpl.param({
	     'true' => {'hoge' => 'ほげほげ'}
	     }) 
  print "Content-Type: text/html\n\n"
  print tmpl.output

実行結果は以下のようになり、param メソッドで指定されなかった部分は表示されません。

  <html>
  <body>

  こちらは True です。
  ほげほげ
  <hr>
  </body>
  </html>

== 付属サンプルについて

:env.rb , env.html 
	単純な文字列の置換のサンプルです。

:envlist.rb
        環境変数のリストを表示するサンプルです。
        イテレーターと共に使用する場合のサンプルです。

:envlist2.rb
        実行結果は envlist.rb と同様です。
        一括でループを生成する場合のサンプルです。

:nest.rb
        ネストしたループのサンプルです。

== ライセンス,作者

著作権は IKEBE Tomohiro が保持します。
Ruby と同様のライセンスのもとで、自由に利用できます。

IKEBE Tomohiro <ikechin@0xfa.com>

=end
